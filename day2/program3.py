num1 = 100
num2 = num1
print(f"num2 = {num2}")
num2 = 200
print(f"num1 = {num1}")


def function_1():
    print("Inside function")


print(f"function_1 = {function_1}, type = {type(function_1)}")


# function alias
function_2 = function_1
print(f"function_2 = {function_2}, type = {type(function_2)}")

print(f"function_1 =  {function_1}")
