# global
num = 100

print(f"outside of any function num = {num}, type = {type(num)}, address = {id(num)}")


def function_1():
    print(f"inside function 1")
    # local
    name = "steve"
    print(f"num = {num}, address = {id(num)}")
    print(f"name = {name}")

# function_1()


def function_2():
    print("inside function 2")
    # it will create new variable which is local to function 2
    num = 200
    print(f"num = {num}, address = {id(num)}")
    num = 300
    print(f"num = {num}, address = {id(num)}")


# function_2()


def function_3():
    print("inside function 2")
    global num
    num = 1000
    num = '100'
    print(f"num = {num}, type = {type(num)}, address = {id(num)}")


# function_3()
print(f"outside of any function num = {num}, type = {type(num)}")


def test_variable():
    """
    this function shows that when we define a variable and without using it if we redefine it and assign some new
    value, it shows some warning and of course it doesn't make any sense why we would define a variable and redefine it
    without using it.
    """
    num2 = 'test'
    print(f"num2 = {num2}")  # if we comment this it would show warning for the num2
    num2 = 'test2'
    # num3 = num2 + 'test3'
    print(f"num2 = {num2}")
    

# test_variable()
