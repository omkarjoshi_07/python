def function_1(name="no name", age=20):
    """
    This function has default arguments
    """
    print("Inside function 2")
    print(f"name = {name}, type = {type(name)}")
    print(f"age = {age}, type = {type(age)}")


# function_1() #prints default arguments
# function_1("person2")# name=person2, age=20
# function_1("person",30) #overwrite default arg with new ones
# function_1(25)# 25 is assigned to name

# positional parameters
# name = person3, age=20
# function_1(name="person3")

# name=person4,age=30
# function_1(name="person4",age=30)

# name="", age=40
# function_1(age=40)

def function_2(p1, p2, p3):
    print("inside function 2")
    print(f"p1 = {p1}")
    print(f"p2 = {p2}")
    print(f"p3 = {p3}")


function_2(10, 20, 30)
function_2(10, 20, p3=30)


def function_3(empid=0, company="", location=""):
    print(f"Emp ID: {empid}")
    print(f"Company: {company}")
    print(f"Localtion: {location}")


# function_3(empid = 200)
function_3(company="WURTH-IT", empid=234, location="Baner, Pune")


# note: If we have 10 params in function and want to pass only specific params in function call
#       1st all params should have defuolt values and we can pass positional params in any sequence

def function_4(company_name, location, technology):
    print("inside the function 4")
    print(f"company = {company_name}")
    print(f"company_location = {location}")
    print(f"Tech = {technology}")


# function_4(company_name="Wuerth-it", location="Baner, Pune") #error,
# we must have default arguments assigned to pass positional parameters.


def function_5(name, age=25):
    print("inside the function 5")
    print(f"name = {name}, age = {age}")
#
#
# function_5("xyz")


# note: whether we pass or not pass default parameters we must pass non default parameters


def function_6(n1, n2, n3=0, n4=0):
    pass


function_6(n1=20,n2=30)






