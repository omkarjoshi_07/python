import pandas as pd
import mysql.connector


def create_employee():
    empid = int(input("Employee ID :"))
    name = input("Name : ")
    department = input("Department : ")
    salary = float(input("Salary : "))
    location = input("Location : ")

    # open mysql connection
    connection = mysql.connector.connect(host='localhost', port=3306, user='root', password='new-password',
                                         database='python_test')
    # Get the cursor (in memory object) from connection
    cursor = connection.cursor()

    # perform the operation
    query = f"insert into employee (empid, name, department, salary, location) values({empid}, '{name}', '{department}', " \
            f"{salary}, '{location}');"

    cursor.execute(query)

    # Commit the changes to the database
    connection.commit()

    # close the cursor
    cursor.close()

    # close the connection
    connection.close()


# create_employee()


def get_employee_details():
    # open mysql connection
    connection = mysql.connector.connect(host='localhost', port=3306, user='root', password='new-password',
                                         database='python_test')
    # Get the cursor (in memory object) from connection
    cursor = connection.cursor()

    query = f"select * from employee;"
    cursor.execute(query)

    # Get the employees
    employees = cursor.fetchall()
    # print(employees)

    list_employees = []
    for e in employees:
        list_employees.append({
            "empid": e[0],
            "name": e[1],
            "department": e[2],
            "salary": e[3],
            "location": e[4]
        })

    df = pd.DataFrame(list_employees)
    print(df)
    cursor.close()
    connection.close()


# get_employee_details()


def update_employee():
    new_location = input("New Location:")
    # open mysql connection
    connection = mysql.connector.connect(host='localhost', port=3306, user='root', password='new-password',
                                         database='python_test')
    # Get the cursor (in memory object) from connection
    cursor = connection.cursor()

    # In query arguments should be same as column names in the database.
    # argument name like empid can be in any case (upper or lower) as database can accept that.
    query = f"update employee set location='{new_location}' where empid=1"
    cursor.execute(query)

    # Commit the changes to the database
    connection.commit()

    # close the cursor
    cursor.close()

    # close the connection
    connection.close()


# update_employee()


def delete_employee():
    # open mysql connection
    connection = mysql.connector.connect(host='localhost', port=3306, user='root', password='new-password',
                                         database='python_test')
    # Get the cursor (in memory object) from connection
    cursor = connection.cursor()
    query = f"delete from employee where empid=3;"
    cursor.execute(query)

    connection.commit()

    cursor.close()
    connection.close()


# delete_employee()
