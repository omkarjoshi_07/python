# We are importing Flask class from flask package
from dbus.decorators import method
from flask import Flask

# Instantiate Flask class and 1st argument is module or package
app = Flask(__name__)


# By default method is GET but we can specify the method
@app.route("/", methods=["GET"])
def root():
    return "<h2>Welcome to flask application</h2>"


@app.route("/products", methods=["GET"])
def get_products():
    return "<h3> List of products </h3>"


@app.route("/category", methods=["GET"])
def get_category():
    return "Category 1"


# running the flask application
# Use of debugger here is to refresh the server automatically after making changes in the code.
# app.run(host="localhost", port=5000, debug=True)
# host="0.0.0.0" tells application to run at all public ip's. So we in this case we have to hit the public
# ip for this machine
app.run(host="0.0.0.0", port=5000, debug=True)
