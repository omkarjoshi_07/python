# Closure

num = 100


def outer_function():
    print("Inside the outer function")
    my_var = 20

    print(f"num = {num}")

    def inner_function():
        print("Inside the inner function")
        my_var2 = "test"

        print(f"num = {num}")
        print(f"my_var = {my_var}")
        print(f"my_var2 = {my_var2}")

    inner_function()


# outer_function()

# Closure
def my_closure(my_decorator):
    print("Inside the my closure ")

    def inner_function():
        print("Inside the inner function")
    my_decorator()
    return inner_function


# result = my_closure()
# print(f"result = {result}, type = {type(result)}")


@my_closure
def my_decorator():
    print("Inside my decorator")


my_decorator()

