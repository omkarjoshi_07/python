from flask import Flask, jsonify
import mysql.connector

app = Flask(__name__)


@app.route("/", methods=["GET"])
def root():
    return "<h2. Welcome to Flask Home page< /h1>"


@app.route("/employees", methods=["GET"])
def get_employees():
    connection = mysql.connector.connect(host="localhost", port=3306, user="root", password="new-password",

                                         database="python_test")
    cursor = connection.cursor()
    query = f"select * from employee;"
    cursor.execute(query)

    employees = cursor.fetchall()
    list_emps = []
    for e in employees:
        list_emps.append({
            "Emp ID": e[0],
            "Name": e[1],
            "Department": e[2],
            "Salary": e[3],
            "Location": e[4]
        })

    cursor.close()
    connection.close()

    return jsonify(list_emps)


app.run(host="localhost", port=5000, debug=True)
