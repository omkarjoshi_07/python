def my_decorator(func):
    print("Inside my_decorator")

    def inner_function():
        print("Inside inner function")
    func()

    return inner_function

@my_decorator
def add():
    n1 = int(input("Enter number 1"))
    n2 = int(input("Enter numner 2"))
    print(f"Addition is : {n1 + n2}")


# add()

# Sequence of decorator
# 1. Calls my_decorator()
# 2. Calls actual function add() in this case

@my_decorator
def multiply():
    n1 = int(input("Enter num 1 : "))
    n2 = int(input("Enter num 2 : "))
    print(f"Multiplication : {n1 * n2}")


multiply()
