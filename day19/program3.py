# pandas data frame
import numpy as np
import pandas as pd

def function_1():
    persons = pd.DataFrame([
        {'Name of Person' : "Raj", "Age": 40, "Infected" : "No"},
        {'Name of Person': "Ram", "Age": 45, "Infected": "Yes"},

    ])
    # print(f"Type of persons : {type(persons)}")
    # print(persons)
    # to get more information about data frame
    # print(persons.describe())
    # print("*" * 20)
    # print(persons.info())


    # numbers  = [1, 45, 456, 7]
    # df1 = pd.DataFrame(numbers, index=['nilesh', 'rahul', 'sumit', 'suraj'])
    # print(f"df1 : {df1} \ntype : {type(df1)}")

    # print(persons.Name)
    print(persons['Name of Person'])

    # Note : To get the specific column if it has special characters like spaces we should use subscript
    # otherwise we can use (.) dot operator

# function_1()

def function_2():
    cars = [{'Model' : "i20", 'Price' : 6.5, 'Color' : 'Black', 'Company' : 'Hyundai'},
            {'Model' : "Alto", 'Price' : 4.5, 'Color' : 'White', 'Company' : 'Maruti Suzuki'},
            {'Model': "Rapid", 'Price': 8.5, 'Color': 'Black', 'Blue': 'Scoda'}
            ]
    df1 = pd.DataFrame(cars)
    # print(df1)
    # print(df1.Model)
    # print(df1['Model'])
    


function_2()

def function_3():
    data = pd.read_csv('C:/Users/omkar/Desktop/test.csv')
    print(f"data : {data}")
    print()
    # print(f"Size : {data.size}")
    # print(f"Shape : {data.shape}")
    # print(f"ndim : {data.ndim}")
    # print(f"Values : {data.values}")

    # print(f"Items : {data.items}")
    # print(f"Columns : {data.columns}, type : {type(data.columns)}")

    # To get 1st 2 records
    # print(f"First 10 records : \n{data.head(10)}")

     # last records
    # print(f"last few records : {data.tail(10)}")

    # Error
    # print(f"data[0] : {data[0]}")

    # Below method returns True if value is NaN
    # print(f"Nan Values : \n{data.isna()}")

    # Get more info about data
    print(f"Description : \n")
    print(data.describe())
    print(f"Info : \n")
    print(data.info())

# function_3()

def function_4():
    cars = [
        {"model": "i20", "price": 6.5, "color": "gray", "company": "hyudai"},
        {"model": "i10", "price": 4.5, "color": "white", "company": "hyudai"},
        {"model": "X5", "price": 36, "color": "black", "company": "bmw"},
        {"model": "nano", "price": 2.5, "color": "yellow", "company": "tata"}
    ]
    # Aply confitions to data frame
    df = pd.DataFrame(cars)
    # print(df)
    # Here if condition does not matches with all columns, it will show only column names and empty dataframe
    print(df[df['price'] > 20])



function_4()

