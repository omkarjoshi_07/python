# pandas
import numpy as np
import pandas as pd

def function_1():
    list_1 = [12, 34, 56, 87]
    print(f"List_1 : {list_1}, type : {type(list_1)}")

    arr1 = np.array([12, 43, 54, 788])
    print(f"arr1 : {arr1}, type : {type(arr1)}")

    # dtype = int64
    series1 = pd.Series([12, 43, 67, 89])
    print(f"series1 : {series1}, type : {type(series1)}")

    # dtype = object
    ser2 = pd.Series([12, "xyz", 234,67])
    print(f"ser2 : \n{ser2}, type : {type(ser2)}")



# function_1()


def function_2():
    series1 = pd.Series([12, 43, 56,89])

    # seperating indexes and values
    print(f"Indexes : {series1.index}")
    print(f"Values : {series1.values}")
    print(f"series[0] : {series1[0]}")

    # below statement will give keyerror as -a index is not avalable inside the series
    # print(f"series[-1] : {series1[-1]}")


# function_2()


def function_3():
    cars = ["i24", "i10", "alto", "Nano"]
    avg = [15, 18, 20, 15]

    # ser1 = pd.Series(cars)
    # print(f"ser1 : {ser1}")


    ser1 = pd.Series(cars, index=avg) # ok
    # ser1 = pd.Series(data=cars, index=avg) #ok
    print(f"cars and Avg : {ser1}")

    print(f"Avg is 18 of Car : {ser1[18]}")
    print(f"Avg is 15 of Car : {ser1[15]}")
    print()

    ser2 = pd.Series(avg, cars)
    print(f"ser2 : {ser2}")
    print(f"Alto's Avg : {ser2['alto']}")

    print()

    dict1 = {'alto' : 15, 'nano' : 20}
    # In below statement, we have passed dict as data to series where keys in the dict will be assigned as indexes
    ser3 = pd.Series(dict1)
    print(f"ser3 : {ser3}")


# function_3()


def function_4():
    ser1 = pd.Series(['Alto', 'Ecosport'], index=[1, 1])
    # print(f"ser1 : {ser1}")
    # print(f"ser1[1] : {ser1[1]}")

    # Fabia is added with index 2
    ser1[2] = 'Fabia'
    print(f"ser1 : {ser1}")

    ser2 = pd.Series((12, 34, 56, 877), ('ad', 'sdfd', 'dg', 'sdf'))
    # print(f"ser2 : {ser2}")
    ser2['a'] = 2344
    print(f"ser2 : {ser2}")



# function_4()


def function_5():
    """Creating list by using a dictionary"""
    ser1 = pd.Series({'name ' : 'xyz', 'age':26, 'email':'xyz@gmail.com'})
    # print(f"ser1 : {ser1}")
    print(f"Indexes : {ser1.index}")
    print(f"Valuues : {ser1.values}")
    # int
    print(f"Type of age : {type(ser1['age'])}")
    print(f"dtype  {ser1.dtype}")
    print(f"ndim : {ser1.ndim}")
    print(f"shape : {ser1.shape}")
    print(f"size : {ser1.size}")


# function_5()