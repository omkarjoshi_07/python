import numpy as np
import pandas as pd

def function_1():
    df = pd.read_csv('./Salary_Data.csv')
    # print(df)

    # Add a column
    df['Anual_Salary'] = df['Salary'] * 12
    # print(df)

    # remove column
    # axis = -> rows
    # axis = 1 -> columns
    # Below statment will drop the column but not at same memory location
    # new_df = df.drop('YearsExperience', axis=1)
    # print(new_df)

    # As per below statement, column will not get deleted fromhard disk i.e. in excel file it will remain as it is.
    df.drop('YearsExperience', axis=1, inplace=True)
    # print(df)

    # Write changes to the hard disk
    # If below file is already exists, it will overwrite the data.
    # df.to_csv('Updated_Salarie_Data.csv')


# function_1()


def function_2():
    df = pd.read_csv('./nba.csv')
    # print(df.columns)

    # To get first 10 rows
    # print(f"1st 10 rows : {df.head(10)}")

    # Filter data -> get those rows where salaries > 1000000

    # To pass multiple columns we need to use subscript([])
    # print(f"Data where Salary > 1000000 \n : {df[df['Salary'] > 2000000][['Name', 'Team', 'Salary']]}")
    df2 = df[df['Salary'] > 2000000][['Name', 'Team', 'Salary']]
    # print(df[['Name', 'Salary']])
    df2.to_csv('player_earning_20m.csv')


function_2()