# indexing, slicing
import numpy as np

def function_1():
    arr1 = np.array([10, 20, 40, 98, 78, 66, 45])
    # array is always immutable
     # broadcast operators
    print(f"arr1 + 10 : {arr1 + 10}")
    print(f"arr1 - 10 : {arr1 - 10}")
    print(f"arr1 * 10 : {arr1 * 10}")


# function_1()


def function_2():
    # Indexing -> positive and negative
    arr1 = np.array([10, 20, 40, 98, 78, 66, 45])
    # Poistive indexing

    print(f"arr1[0] : {arr1[0]}")
    print(f"arr1[4] : {arr1[4]}")
    print(f"arr1[3] : {arr1[3]}")

    # negative indexing
    print(f"arr1[-1] : {arr1[-arr1.size]}")
    print(f"arr1[-2] : {arr1[-2]}")
    print(f"arr1[-4] : {arr1[-4]}")

    # index = -1
    # for i in range(0, arr1.size):
    #
    #     print(arr1[index])
    #     index -= 1




# function_2()


def function_3():
    # slicing - getting portion of array
    arr1 = np.array([10, 20, 40, 98, 78, 66, 45])

    # slicing which is also used in the lst collection
    print(f"arr1[2:5] : {arr1[2:5]}")
    print(f"arr1[:5] : {arr1[:5]}")
    print(f"arr1[2:] : {arr1[2:]}")
    print('*' * 20)

    # numpy specific slicing
    print(f"Ele at position 2, 6, 4 : {arr1[[2, 6, 4]]}")
    print(f"Ele at position 1, -2, -3 : {arr1[[1, -2, -3]]}")
    print(f"arr1[:] : {arr1[:]}")

    # Below statement would give the result as only those numbers at indexes where we have passed True value
    # Filtering
    print(f"arr1[False, True, False, True, True, False, False : {arr1[[False, True, False, True, True, False, False]]}")
    print(f"arr1 > 50 : {arr1  > 50}")
    print(f"arr1 > 50 : {arr1[[False, False, False, True, True, True, False]]}")
    print(f"arr1 > 50 : {arr1[arr1 > 50]}")





# function_3()


def function_4():
    salaries = [20, 40, 45, 50, 60, 21, 55, 90, 88, 70, 84]
    print(f"Filtered Salaries : {list(filter(lambda s: s > 50, salaries))}")
    print(f"Filtered Salaries : {[s for s in salaries if s > 50]}")

    salaries2 = np.array(salaries)
    print(f"Filtered Salaries : {salaries2[salaries2 > 50]}")

    # print salaries ranging between 30k to 60k
    print(f"salaries between 20k to 60k : {salaries2[(salaries2 > 20) & (salaries2 < 60)]}")

# function_4()

