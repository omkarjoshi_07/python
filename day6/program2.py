def function_1():
    str_1 = "this &is &a &test &string"
    # str_1 = str_1.replace('&', '')
    # print(f"str_1 = {str_1}")

    list_1 = list(str_1)
    list_1.pop(list_1.index('&'))

    str_1 = ''.join(list_1)
    print(f"str_1 = {str_1}")


# function_1()


def function_2():
    '''
    This is test docstring
    '''
    filename = "myimage.jpg"
    parts = filename.split('.')
    print(f"filename = {parts[0]}")
    print(f"Extension = {parts[1]}")

# function_2()
# print(function_2.__doc__)

def function_3():
    """
    removes unwanted characters from string
    """
    string = "this $is ^a &&test @string 1. this is a ~@^test string 2. this is a test string 3."
    print(f"is my word present = {'my' in string}")
    print(f"is test word present = {'test' in string}")

    unwanted_characters = "~!@#$%^&*()"
    result_string =[]
    for character in string:
        if character not in unwanted_characters:
            result_string.append(character)
    string = ''.join(result_string)

    print(f"Final string after removing unwanted characters = {string}, type: {type(string)}")


# function_3()


def function_4():
    string = "this $is ^a &&test @string 1. this is a ~@^test string 2. this is a test string 3."
    unwanted_characters = "~!@#$%^&*()"

    for ch in unwanted_characters:
        string = string.replace(ch, '')

    print(f"Final string after removing unwanted characters = {string}")

# function_4()


def function_5():
    '''
    fill method is provided to fill up zeros in the string
    '''
    string = "this is test string"
    print(f"string = {string}")
    print(f"final string = {string.zfill(30)}")


# function_5()


