# string
def function_1():
    str_1 = "This is A Test string 1"
    print(f"str_1 = {str_1}")

    num = 100
    str_num = str(num)
    print(f"str_num = {str_num}, type = {type(str_num)}")

    str_1_uppercase = str_1.upper()
    print(f"str_1_uppercase = {str_1_uppercase}")

    str_1_lowercase = str_1.lower()
    print(f"str_1_lowercase = {str_1_lowercase}")

    print(f"address of str_1 = {hex(id(str_1))}")
    # print(f"address after uppercase conversion = {hex(id(str_1.lower()))}")
    # str_1 = str_1.lower()

    print(f"length of str_1 = {len(str_1)}")


# function_1()


def function_2():
    """convert string into reversed order"""
    str_1 = "This Is A   Test   String 1"
    list_str = list(str_1)

    print(f"characters = {list_str}")
    list_str.reverse()
    print(f"reversed characters = {list_str}")
    print(f"reversed string = {''.join(list_str)}")
    print(f"Test string: {''.join(['o', 'm', 'k', 'a', 'r'])}")

# function_2()


def function_3():
    """
    convert list to string using various join operations
    """
    list_1 = ['t', 'h', 'i', 's']
    print(f"string_1 = {''.join(list_1)}")
    print(f"string_1 = {'-'.join(list_1)}")
    print(f"string_1 = {'*-*'.join(list_1)}")


# function_3()


def function_4():
    """
    removing spaces
    """
    str_1 = "   test string     "
    print(f"str_1 = {str_1}")
    print(f"left trimmed str_1 = {str_1.lstrip()}")
    print(f"right trimmed str_1 = {str_1.rstrip()}")
    print(f"all trimmed str_1 = {str_1.strip()}")

# function_4()


def function_5():
    str_1 = "cdac"
    str_2 = "cdac acts"
    str_3 = "cdac acts ATC"

    # print(f"str_1 = {str_1}, str_2 = {str_2}")
    # print("str_1 = {str_1}, str_2 = {str_2}")

    print("str_1 = {0}, str_2 = {1}, str_3 = {2}".format(str_1, str_2, str_3))

    # left aligned and spaces thereafter
    print("str_1 = [{0:<20}], str_2 = [{1:<30}]".format(str_1, str_2))

    # right aligned and spaces thereafter
    print("str_1 = [{0:>20}], str_2 = [{1:>30}]".format(str_1, str_2))
    print(f"str_1 = [{0:>20}], str_2 = [{1:>30}]".format(str_1, str_2)) # meaning of 0 & 1 changes when f is used.

    # center aligned
    print("str_1 = [{0:^20}], str_2 = [{1:^30}], str_3 = [{2:^25}]".format(str_1, str_2, str_3))


# function_5()


def function_6():
    """
    This function converts int into decimal,binary, hexadecimal

    """
    number = 100

    # print number having +ve symbol
    print("decimal number +ve = {0:+}".format(number))

    # print number having -ve symbol
    print("decimal number -ve = {0:-}".format(number))

    print("decimal number = {0:n}".format(number))
    print("decimal number = {0:d}".format(number))

    # binary
    print("decimal number in binary = {0:b}".format(number))

    # octal
    print("decimal number in octal = {0:o}".format(number))

    # lower hexa-decimal
    print("decimal number in lower hexadecimal = {0:x}".format(number))

    # upper hexadecimal
    print("decimal number upper hexadecimal = {0:X}".format(number))


# function_6()


def function_7():
    number = 3455.23445444
    print("number = {0}".format(number))

    # upto 2 decimal places
    print("number upto 2 decimal places = {0:.2f}".format(number))
    # print("{0:.2f}".format(number))

    print("number in scientific format = {0:e}".format(number))

    print("number in scientific format = {0:E}".format(number))


# function_7()

def function_8():
    str_1 = 'python'
    str_2 = '120'

    print(f"isalpha str_1 = {str_1.isalpha()}, str_2 = {str_2.isalpha()}")
    print(f"isnumeric str_1 = {str_1.isnumeric()}, str_2 = {str_2.isnumeric()}")
    print(f"islower str_1 = {str_1.islower()}, str_2 = {str_2.islower()}")

# function_8()


def function_9():
    string = "PG DAC"
    # basically when we say {0:>10} it calculates lenght of string and substract it fro 10 and gives remaining diff as space
    print("string = Advance computing in {0:>10}".format(string))
    print(f"string = {string : >10}")

# function_9()



def function_10():
    """
    splitting string
    """
    string = "red,green,yellow,black"
    colors = string.split(',')
    print(f"colors = {colors}, type = {type(colors)}")

    timestamp = "15-04-2021T10:34 am"

    timestamp_parts = timestamp.split('T')
    print(f"timestamp_parts = {timestamp_parts}")

    date = timestamp_parts[0]
    date_parts = date.split('-')
    print(f"date_parts = {date_parts}")

    time = timestamp_parts[1]
    time_parts = time.split(' ')
    print(f"time_parts = {time_parts}")

    hours_minutes = time_parts[0].split(':')
    print(f"hours_minutes = {hours_minutes}")

    # printing date and time parts
    print(f"day : {date_parts[0]}, month : {date_parts[1]}, year : {date_parts[2]}, hour : {hours_minutes[0]}, minutes : {hours_minutes[1]}, merdian : {time_parts[1]}")


# function_10()


def function_11():
    str_1 = "This %is a $test @string"
    str_1 = str_1.replace('%', '')
    print(f"str_1 = {str_1}")

    str_1 = str_1.replace('$', '')
    print(f"str_1 = {str_1}")

    str_1 = str_1.replace('@', '')
    print(f"str_1 = {str_1}")

    str_1 = str_1.replace('test', 'replaced')
    print(f"str_1 = {str_1}")


function_11()

