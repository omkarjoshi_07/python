def remove_unwanted_chars():
    string_1 = "This @@is $$function $which converts string into&& proper** format"
    converted_list = list(string_1)
    # count = converted_list.count('$')
    # remove_range = range(0, count)
    for index in range(0, converted_list.count('$')):
        converted_list.remove('$')
    # converted_list.pop(converted_list.index('$'))
    # converted_list.pop(converted_list.index('@'))
    # converted_list.pop(converted_list.index('&'))
    # converted_list.pop(converted_list.index('*'))
    # print(f"converted-list = {converted_list}")
    string_1 = ''.join(converted_list)
    print(f"string_1 = {string_1}")


# remove_unwanted_chars()


def function_2():
    str_1 = "Hello, pythons here"
    str_2 = str_1.split(",")
    print(f"str_2 = {str_2}, type of str_2 = {type(str_2)}")


# function_2()


def check_string():
    str_1 = "this is simple python application"
    # print(f"simple word is present on = {'simple' in str_1}")
    if 'this is simple' in str_1:
        print(f"sentence found..")
    else:
        print(f"sentence not found...")


# check_string()


def remove_unwanted_chars_from_string():
    """
        This function removes unwanted characters from string and print them
    """
    string_1 = "This $is ^a &&test @string 1. this is a ~@^test string 2. this is a test string 3."
    unwanted_chars = "@#$%&*~^"

    final_string = []
    removed_characters = []
    for character in string_1:
        if character not in unwanted_chars:
            final_string.append(character)
        else:
            removed_characters.append(character)
    print(f"Removed characters : {''.join(removed_characters)}")
    print(f"Final string after removing characters : {''.join(final_string)}")
    # print(string_1.find("@"))
    # print(string_1.count("@"))
    # print(string_1.capitalize())


remove_unwanted_chars_from_string()
