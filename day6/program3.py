def function_1():
    str_1 = "this is a TEST string"

    # 1st letter capital
    print(f"string in 1st capital = {str_1.capitalize()}")

    # make all characters lower case
    print(f"string in lower case = { str_1.casefold()}")
    print(f"string in lower case 2 = { str_1.lower()}")

    # count no of occurence of substring
    print(f"count of 'is' in str_1 = {str_1.count('is')}")

    # check if string ends with some value
    print(f"string ends with 'is' = {str_1.endswith('is')}")
    print(f"string ends with string = {str_1.endswith('string')}")

    print(f"find position of is = {str_1.find('is')}")


function_1()
