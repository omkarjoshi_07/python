
def reverse_string_1():
    string_to_reverse = input('Enter the string')
    list_of_chars = list(string_to_reverse)
    print(f"List of chars = {list_of_chars}")
    list_of_chars.reverse()
    print(''.join(list_of_chars))


# reverse_string_1()


def remove_spaces():
    test_string = " This is string with spaces    "
    print(f"Left trimmed string = {test_string.lstrip()}")
    print(f"Right trimmed string = {test_string.rstrip()}")
    print(f"Trimmed with all spaces = {test_string.strip()}")


# remove_spaces()

def format_string():
    test_string_1 = "Wurth"
    test_string_2 = "Wurth IT"
    test_string_3 = "Hinjewadi, Pune"

    # below print statement should not contain f
    print("test_string_1 = {0}, test_string_2 = {1}, test_string_3 = {2}".format(test_string_1, test_string_2,
                                                                                 test_string_3))
    # In below statement {0} = test_string, {1} = test_string_2 # left aligned
    print("test_string_1 = [{1:<30}], test_string_2 = [{0:<30}]".format(test_string_1, test_string_2))

    # right aligned
    print("test_string_1 = [{1:>30}], test_string_2 = [{0:>30}], test_string_3 = [{2:>30}]".format(
        test_string_1, test_string_2, test_string_3))

    # center aligned
    print("test_string_1 = [{1:^30}], test_string_2 = [{0:^30}]".format(test_string_1, test_string_2))


# format_string()
