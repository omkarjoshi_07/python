test_var = lambda p1: p1 + 10
# print(test_var(10))
# print(type(test_var))

test_var_2 = lambda: print("in lambda function")
# test_var_2() # lambda call


test_var_3 = lambda param: print(param)
# test_var_3(50)
# test_var_3("This is parameter passed to the lambda function")
# test_var_3(test_var(20))

monthly_salary = lambda sal: sal
# print(monthly_salary(30000.00))

annual_sal = lambda: monthly_salary(30000.00) * 12
print(f"Annual Salary = {annual_sal()}")


dummy_lambda = lambda param = 200: print(f"Default argument in lambda = {param}")
print(dummy_lambda())
