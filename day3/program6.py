test_lambda = lambda: print("I am lambda expression without any parameters.")
# print(f"{test_lambda()}")

test_lambda()

moduli_lambda = lambda num1, num2: num1 % num2
print(f"modulus : {moduli_lambda(10, 5)}")

nested_lambda = lambda val: moduli_lambda(10, 5) + val
print(f"nested lambda value : {nested_lambda(100)}")


def add(num1, num2):
    return num1 + num2


add_lambda = lambda param: add(10, 40) + param

print(f"value added from lambda : {add_lambda(40)}")


def substract(num):
    return add_lambda(10) - num


print(f"Substracted value : {substract(30)}")

multiply_lambda = lambda param=5: substract(30) * param

print(f"multiplied value : {multiply_lambda()}")
