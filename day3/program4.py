def function_1():
    numebers = [10, 20, 30, 40, 50]
    # print(numebers)

    # for loop
    for ele in numebers:
        print(f"value = {ele}, type = {type(ele)}")
# function_1()


def list_operation_1():
    numbers = [10, 20, 30, 40, 50]
    print(numbers)

    #append
    numbers.append(60)
    print(numbers)

    numbers.append(70)
    print(numbers)

    # remove last value
    pooped_value = numbers.pop()
    print(f"pooped value = {pooped_value}")
    print(numbers)

    pooped_value = numbers.pop()
    print(f"pooped value = {pooped_value}")
    print(numbers)


# list_operation_1()

def list_operation_2():
    numbers = [10, 20, 30, 40, 50]
    print(numbers)

    #insert in between
    numbers.insert(1, 100)
    print(numbers)

    #insert adds ele at index provided
    numbers.insert(2, 200)
    print(numbers)

    print(f"{numbers.pop(1)} is popped")
    print(numbers)
    print(f"{numbers.pop(3)} is popped")

    print(numbers)
    # numbers.insert(300)
    # print(numbers)


# list_operation_2()


def list_operation_3():
    numbers = [10, 20, 30, 40]
    print(f"length of numbers = {len(numbers)}")

    numbers.append(50)
    print(f"length of numbers = {len(numbers)}")

    numbers.pop()
    print(f"length of numbers = {len(numbers)}")



# list_operation_3()