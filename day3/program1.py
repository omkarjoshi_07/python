def add(n1, n2):
    return n1 + n2


# print(f"Addition is {add(2,3)}")

def cal_modulus(num1, num2):
    return num1 % num2


# print(f"{cal_modulus(12, 2)} ")
if cal_modulus(12, 4) == 0:
    print(f"modulus is true")
else:
    print(f"modulus is false")


def square(n1, n2):
    return n1 * n2


# print(f"Square = {square(2,3)}")

# lambda -> unnamed/anonymous function
add_lambda = lambda n1, n2: n1 + n2


print(f"Addition = {add_lambda}, type = {type(add_lambda)}")

# lambda function call
# addition = add_lambda(10,20)
# print(f"Addition = {addition}")

annual_salary = lambda sal: sal * 12
print(f"Annual Salary = {annual_salary(15000.00)} per anum")

# dummy lambda
# dummy_lambda = lambda  p1 : print(f"p1 = {p1}")
# print(f"dummy lambda = {dummy_lambda(20)}")

# Note: We cannot define lambda withhin lambda but we can call lambda within lambda

# nested lambda
add_bonus = lambda bonus: annual_salary(15000.00) + bonus
print(f"Annual salary with bonus = {add_bonus(15000)}")

# lambda function without parameters
test_lambda_1 = lambda: print("this is lambda function without taking parameter")
test_lambda_1()

test_lambda_2 = lambda param=20: print("this is lambda function with default argument")
test_lambda_2()
