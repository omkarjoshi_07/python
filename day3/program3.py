#collections


def function_1():
    numbers_1 = [] # empty list
    print(numbers_1)
    print(f"number_1 = {numbers_1} type = {type(numbers_1)}")

    numbers_2 = list()
    print(f"number_2 = {numbers_2} type = {type(numbers_2)}")


# function_1()

def function_2():
    numbers_1 = [10, 20, 30, 40, 50]
    print(f"number_1 = {numbers_1} type = {type(numbers_1)}")

    numbers_2 = list([1, 12, 3, 4, 66, 77])
    print(f"number_2 = {numbers_2} type = {type(numbers_2)}")

# function_2()


def function_3():
    countries = ["india", "china", "japan", "USA"]
    print(f"countries = {countries} type = {type(countries)}")

    # list of strings
    streams = ['mech', 'EE', 'ENTC']
    print(f"streams = {streams} type = {type(streams)}")


# function_3()

# mixed values in list
def function_4():
    mixed_values = [10, 20, "india", False, 30.45]
    print(f"mixed_values = {mixed_values} type = {type(mixed_values)}")


function_4()
