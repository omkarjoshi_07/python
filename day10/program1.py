class Laptop:
    """
    dummy class
    """
    pass


# instantiation
l1 = Laptop()
print(f"size of l2 before initialization = {l1.__sizeof__()}")


# state = attributes
print(l1.__dict__)
setattr(l1, "processor", "i3")
setattr(l1, "graphics", "RADEON")
setattr(l1, "price", 30000.00)
# print(l1.__dict__)

print(f"Processor = {getattr(l1, 'processor')}")
print(f"graphics = {getattr(l1, 'graphics')}")
print(f"price = {getattr(l1, 'price')}")


l2 = Laptop()
setattr(l2, "Brand", "HP")
setattr(l2, "Generation", "3rd")
setattr(l2, "RAM", "8GB")

print(f"l2 = {l2.__dict__}")

print(f"size of l1 = {l1.__sizeof__()}")
print(f"size of l2 = {l2.__sizeof__()}")


# python allows flexibility in terms of attributes i.e we can have different attributes in
# different objects

