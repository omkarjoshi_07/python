class Car:
    """
    This class has methods printing car details and init has methods
    """
    # def init_car(self, Model, Brand, Price):
    #     setattr(self, 'Model', Model)
    #     setattr(self, 'Brand', Brand)
    #     setattr(self, 'Price', Price)

    def __init__(self, model = 'Rapid', brand = 'Rapid', price = 800000.00):
        setattr(self, 'Model', model)
        setattr(self, 'Brand', brand)
        setattr(self, 'Price', price)

    def print_details(self):
        print(getattr(self, 'Model'))
        print(getattr(self, 'Brand'))
        print(getattr(self, 'Price'))


# car_1 = Car("Nano", "TATA", "150000.00")
car_1 = Car()
# car_1.init_car('Alto 800', 'Maruti Suzuki', 400000.00)
car_1.print_details()
