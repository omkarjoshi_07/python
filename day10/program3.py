# using methods in class

class Person:

    # method
    def init_person(self, name, address, age):
        setattr(self, "name", name)
        setattr(self, "address", address)
        setattr(self, "age", age)

    def print_person(self):
        print(f"Name : {getattr(self, 'name')}")
        print(f"Address : {getattr(self, 'address')}")
        print(f"Age : {getattr(self, 'age')}")

    def can_vote(self):
        if getattr(self, 'age') > 18:
            print(f"{getattr(self, 'name')} can vote")
        else:
            print(f"{getattr(self, 'name')} can not vote")


p1 = Person()
p1.init_person('Sandeep', 'Pune', 30)
p1.print_person()
p1.can_vote()

print(p1.__dict__)
print(Person.__dict__)

print("-" * 70)
p2 = Person()
p2.init_person('rahul', 'Mumbai', 17)
p2.print_person()
p2.can_vote()
print(p2.__dict__)
print(Person.__dict__)
print("-" * 70)


def print_person(num):
    print(f"num = {num}")


print_person(12)

# here methods of class are at some memory location
# for every object address/location of methods of class is same
