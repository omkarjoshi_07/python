class Companies:
    """
    dummy class
    """
    pass


company_1 = Companies()
setattr(company_1, "Name", "Xcaliber")
setattr(company_1, "Location", "Pavillion Mall, Pune")

print(company_1.__dict__)
print("-" * 70)
# here one more object is created.
# company_1 = Companies()
# print(company_1.__dict__)

company_2 = Companies()
setattr(company_2, "Name", "Capegemini")
setattr(company_2, "Location", "Hinjewadi Phase 3, Pune")
print(company_2.__dict__)


class Employee:
    """
    This class has methods to instantiate object and print its details
    """
    pass


def init_employee(employee, Name, Empid, Company, Native_Location):
    setattr(employee, "Name", Name)
    setattr(employee, "Empid", Empid)
    setattr(employee, "Company", Company)
    setattr(employee, "Native_Location", Native_Location)


def print_emp_details(employee):
    print("Employee Details are:")
    print(f"Name of Employee : {getattr(employee, 'Name')}")
    print(f"Employee ID : {getattr(employee, 'Empid')}")
    print(f"Company : {getattr(employee, 'Company')}")
    print(f"Location : {getattr(employee, 'Native_Location')}")


e1 = Employee()
init_employee(e1, "xyz", 101, "BMC", "California")
print_emp_details(e1)
