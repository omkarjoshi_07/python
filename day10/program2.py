class Mobile:
    pass


# by writing below function we can assure that each object of Mobile class will have same attributes/data members
def init_mobile(mobile, company, model, price):
    print("mobile is initialised")
    setattr(mobile, "company", company )
    setattr(mobile, "model", model)
    setattr(mobile, "price", price)


def print_mobile(mobile):
    print("Mobile Details are : \n")
    print(f"Company : {getattr(mobile, 'company')}")
    print(f"Model : {getattr(mobile, 'model')}")
    print(f"Price : {getattr(mobile, 'price')}")


def check_price(mobile):
    if getattr(mobile, 'price') > 30000:
        print(f"{getattr(mobile, 'model')} is not affordable")
    else:
        print(f"{getattr(mobile, 'model')} is affordable")


m1 = Mobile()

init_mobile(m1, "apple", "iphone 11", 120000.00)
print_mobile(m1)
check_price(m1)
print("-"*70)

m2 = Mobile()
init_mobile(m2, "xaomi redmi", "redmi note 5", 16000.00)
print_mobile(m2)
check_price(m2)


print("-"*70)
m3 = Mobile()
init_mobile(m3, "One Plus", "One plus 8 pro", 38000.00)
print_mobile(m3)
check_price(m3)