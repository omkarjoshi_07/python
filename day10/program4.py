# initializer
# if we have init method and if we are directly calling printperson w/o calling init, it will
# crash because as we have not called init method

class Person:
    # initializer
    def __init__(self, name = '', address = '', age = 0):
        print(f"inside __init__() initializer")
        setattr(self, "name", name)
        setattr(self, "address", address)
        setattr(self, "age", age)

    # def __init__(self):
    #     print("inside parameterless init")

    def print_details(self):
        print(f"Name: {getattr(self, 'name')}")
        print(f"Address: {getattr(self, 'address')}")
        print(f"Age: {getattr(self, 'age')}")


p1 = Person()
p1.print_details()

p2 = Person('ram', 'Ayodhya', 30)
p2.print_details()
