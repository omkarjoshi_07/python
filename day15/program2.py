# type casting

def function_1():
    """
    This function will convert different data types into str
    """
    num = 100
    # str1 = str(num)
    # standard way to convert any data type into str
    str_1 = f"{num}"
    print(f"str = {str_1} type of str_1 = {type(str_1)}")

    can_vote = True
    str_2 = f"{can_vote}"
    print(f"str_2 = {str_2} type of str_2 = {type(str_2)}")


# function_1()


def function_2():
    """
    converts str to int
    """

    # str has to be number
    str_1 = "200"
    num_1 = int(str_1)
    print(f"num_1 = {num_1} type of num_1 = {type(num_1)}")

    str_2 = "10.60"
    num_2 = float(str_2)
    print(f"num_2 = {num_2} type of num_2 = {type(num_2)}")


# function_2()


def function_3():
    numbers_1 = [10, 20, 30, 40, 50, 10, 50, 55, 60, 30]
    tuple_1 = tuple(numbers_1)
    print(f"tuple_1 = {tuple_1} type of tuple_1 = {type(tuple_1)}")

    set_1 = set(numbers_1)
    print(f"set_1 = {set_1} type of set_1 = {type(set_1)}")


# function_3()





