# custom exceptions

# This class must be derived from Exception class
class InvalidAgeError(Exception):
    # def __init__(self):
    #     print("Invalid age entered..")
    pass


def function_1():
    name = input("Enter your name : ")
    age = int(input("Enter your age : "))
    address = input("Enter your address : ")
    if age < 25 or age > 60:
        raise InvalidAgeError

    print(f"Name : {name}")
    print(f"Age : {age}")
    print(f"Address : {address}")


try:
    function_1()
except InvalidAgeError:
    print("Invalid age entered..")


