def function_1():
    try:
        # this code will be first tried out to save our application from crashing
        str1 = input("Enter a number : ")
        num = int(str1)
        print(f"num = {num}, type = {type(num)}")
    except:
        # used to catch the errors.
        # once the error it won't reach to PVM which results in non crashing application.
        print(f"Application has received an error")


# function_1()


def function_2():
    try:
        num1 = int(input("Enter number 1 : "))
        num2 = int(input("Enter number 2 : "))
        result = num1/num2
    except ValueError:
        print(f"Divide by zero exception.. Please try again..")
        # function_2()
    except ZeroDivisionError:
        print("Division by zero error...Denominator cannot be zero")
    except:
        # generic except block
        print("Some other error..")
    else:
        # will get executed when there is no error
        print(f"Result = {result}")


function_2()


def function_3():
    pass



