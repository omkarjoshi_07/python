class Person:
    def __init__(self, name="", age=0, address=""):
        self._name = name
        self._age = age
        self._address = address

    def __del__(self):
        print("Object is deinitialized")

    def print_details(self):
        print(f"Name : {self._name}")
        print(f"Age : {self._age}")
        print(f"Address : {self._address}")

    def __str__(self):
        """
        way to convert object into str
        """
        return f"Name : {self._name}, Age : {self._age}, Address : {self._address}"

    def can_vote(self):
        if self._age > 18:
            print(f"{self._name} is eligible for voting")
        else:
            print(f"{self._name} is not eligible for voting")

    def get_data_from_user(self):
        self._name = input("Name : ")
        self._age = int(input("Age : "))
        self._address = input("Address : ")

    def set_name(self, name):
        self._name = name

    def set_age(self, age):
        self._age = age

    def set_address(self, address):
        self._address = address

    def get_name(self):
        return self._name

    def get_age(self):
        return self._age

    def get_address(self):
        return self._address


p1 = Person()
# p1.print_details()
# p1.get_data_from_user()
# p1.print_details()

num = 100

# behind the scene -> int.__str__(num)
# num.__str__()
# here int is getting converted into str and passed to the print function.
# print(f"num = {num}")


# Person.__str__(p1) -> not available in Person class hence will be checked in object class. -> object.__str__(p1)
# p1.__str__() -> checked in Person class it not available then will be checked in object class because Person
# is derived from object class.
# print(f"p1 = {p1}")

# here Person clas doesn't know how to convert p1 into string as like int class in above example so that
# we will override object.__str__(p1) in Person class

print(f"p1 = {p1}")

