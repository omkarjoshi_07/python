# OOPS,
# empty class
class Person:
     """
     this is empty class
     """
     pass

# c++
# Person p;

# java
# Person p = new Person();

# python

# here person is reference
person = Person()
print(f"person = {person}, type of person = {type(person)}")
print(f"Person = {Person}, type = {type(Person)}")

per1 = person
print(f"person = {person}, type of person = {type(person)}")


# create objects in python
p1 = Person()
p2 = Person()
p3 = Person()

