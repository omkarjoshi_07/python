def print_person(person):
    print(f"name = : {person['name']}")
    print(f"age = : {person['age']}")
    print(f"address = : {person['address']}")


def can_vote(person):
    if person['age'] >= 18:
        print(f"{person['name']} is eligible for voting")
    else:
        print(f"{person['name']} is not eligible for voting")


person1 = {"name": "shybaj", "address": "Yadrav", "age": 25}
person2 = {"name": "vedant", "address": "Ichalkarnji", "age": 14}

can_vote(person1)
can_vote(person2)

print_person(person1)
print_person(person2)

print(f"type of person = {type(person1)}")
print(f"type of print_person = {type(print_person)}")
