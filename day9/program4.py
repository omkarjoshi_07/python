class Headphone:
    """
    This class has headphone attributes
    """
    pass


headphone = Headphone()
# print(f"Type of headphone object : {type(headphone)}, size of class Headhone : {headphone.__sizeof__()}")
setattr(headphone, "Brand", "Oneplus")
setattr(headphone, "Price", 5000.00)
setattr(headphone, "Model", "Oneplus Z2")
setattr(headphone, "Price", "3000.00")
# print(headphone.__dict__)
print(f"Brand Name : {getattr(headphone, 'Brand')}")
print(f"Model : {getattr(headphone, 'Model')}")
print(f"Price : {getattr(headphone, 'Price')}")
