class Employee:
    """
    this is Employee class
    """
    pass


e1 = Employee()

# e1 will have the following attributes name, eid, salary

# add fields or attributes in the object e1
setattr(e1, "name", "emp1")
setattr(e1, "eid", 1001)
setattr(e1, "salary", 20000.00)

print(e1.__doc__)
# print(e1.__dict__)
print(f"Employee Name = {getattr(e1, 'name')}")
print(f"Employee ID = {getattr(e1, 'eid')}")
print(f"Employee Salary = {getattr(e1, 'salary')}")

e2 = Employee()
print(e1.__doc__)

setattr(e2, "fullname", getattr(e1, 'name'))
print(e2.__dict__)

e3 = Employee()
print(e3.__doc__)
