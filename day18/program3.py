import numpy as np

def function_1():
    arr1 = np.array([10, 29, 45, 567, 454, 56])
    print(arr1)
    print(f"dimensions : {arr1.ndim}")
    print(f"How many ele in {arr1.ndim} dimensions or no. of rows : {arr1.shape}")

    arr2 = np.array([[10, 20, 40, 54],
                     [3, 5, 7, 9]])
    # Below statement will print arr2 as shown above
    print(arr2)

    # ndim gives no of dimensions
    print(f"dimensions : {arr2.ndim}")
    # shape gives no of rows and columns like -> (2, 4) -> means 2 rows and 4 columns
    print(f"How many ele in {arr2.ndim} dimensions or no. of rows : {arr2.shape}")

    # Note as we have created 2 dimensional array above, memory will get allocated contigously


# function_1()


def function_2():
    # In below statement, no of ele is different in each dimension i.e. there will be 2 rows and 3 columns -> 1s row
    # and 4 columns in 2nd row which will give 'VisibleDeprecationWarning' (Deprecation warning) so we need
    # to specify the dtype=object in future numpy versions
    arr_1 = np.array([[1, 2, 4], [5, 6, 7, 9]], dtype=object)
    print(f"arr_1 : {arr_1}, type of arr_1 : {type(arr_1)}")


# function_2()

def function_3():
    arr1 = np.array([1, 2, 4, 6, 9, 10])
    # In below statement, , 3 -> 2*3 -> 6 = no of ele in arr1
    # Here we should specify no of rows and no of columns that should be equal to total no. of ele in array
    arr2 = arr1.reshape(2,3)
    print(arr2)
    print(f"dimensions : {arr2.ndim}")
    print(f"How many ele in {arr2.ndim} dimensions or no. of rows : {arr2.shape}")
    print('*' * 50)

    arr3 = arr1.reshape(3, 2)
    print(arr3)
    print(f"dimensions : {arr3.ndim}")
    print(f"How many ele in {arr3.ndim} dimensions or no. of rows : {arr3.shape}")
    print('*' * 50)

    # Below statement will convert arr4 into one row and 6 cols which is still be 2 dimensional array
    arr4 = arr1.reshape(1, 6)
    print(arr4)
    print(f"dimensions : {arr4.ndim}")
    print(f"How many ele in {arr4.ndim} dimensions or no. of rows : {arr4.shape}")


function_3()

