# numpy module
import numpy as np
def function_1():
    numbers_1 = [10, 20, 40, 65]
    print(f"numbers_1 = {numbers_1}, type : {type(numbers_1)}")

    # array
    # np.array() function creates an object of nd.array() class
    numbers_2 = np.array([20, 40, 54, 40])
    print(f"numbers_2 : {numbers_2}, type : {type(numbers_2)}")

# function_1()



def print_array_details(arr1):
    # here ndarray is object which is storing int64 value
    # arr1 = np.array([10, 209, 45, 60])
    print(f"arr1 : {arr1}")
    print(f"type : {type(arr1)}")
    print(f"Array elements data type : {arr1.dtype}")
    print(f"Array ele size : {arr1.itemsize}")
    print(f"Total no of ele : {arr1.size}")
    print(f"total memory consumption : {arr1.itemsize * arr1.size}")

# print_array_details

def function_3():
    numbers = np.array([12, 34, 67, 87, 34], dtype=np.int32)
    print_array_details(numbers)

# function_3()

def function_4():
    numbers = np.array([12.34, 12, 54, 78])
    # print(numbers)
    print_array_details(numbers)

    numbers1 = np.array([12.32, 5.4, 7.6], dtype=np.float32)
    print_array_details(numbers1)

    numbers2 = np.array([12.32, 5.4, 7.6], dtype=np.float16)
    print_array_details(numbers2)

# function_4()

def function_5():
    array1 = np.array(["India", "AUSaddsdd", "UAS"])
    print_array_details(array1)


# function_5()

def function_6():
    a1 = np.array([10, 20, 40, 60])
    # print_array_details(a1)
    print(f"Value at 0th POsition : {a1[0]}")
    # for i in a1:
    #     print(i)

# function_6()


def function_7():
    # We have control over bytes allocated to numpy arrray using 'dtype=np.int16' like wise.
    arr1 = np.array([10, 20, 45, 60], dtype=np.int16)
    print(f"arr1 ele size : {arr1.itemsize}")
    print(f"Total memory taken by arr1 : {arr1.itemsize * arr1.size} bytes")


    import sys

    list1 = [123, 5, 56, 67]
    print(f"list1 ele size : {sys.getsizeof(list1)} bytes")
    print(f"Total memory taken by list1 : {sys.getsizeof(list1) * len(list1)} bytes")


function_7()

