# Using packages from modules

def function_1():
    import mypackage.person
    person1 = mypackage.person.Person("Raj")
    print(person1)

    import mypackage.account
    acc1 = mypackage.account.Account(20000.00, "Savings")
    print(acc1)


# function_1()

def function_2():
    import mypackage.person as myperson
    p1 = myperson.Person("xyz")
    print(p1)

    import mypackage.account as myaccount
    acc1 = myaccount.Account(10000.00, "Current")
    print(acc1)


# function_2()

def function_3():
    from mypackage.person import Person
    p1 = Person("lmn")
    print(p1)

    from mypackage.account import Account
    acc1 = Account(5000, "Saving")
    print(acc1)

# function_3()

def function_4():
    from mypackage.person import Person as MyPerson
    p1 = MyPerson("Nil")
    print(p1)

    from mypackage.account import Account as MyAccount
    acc1 = MyAccount(5000, "Saving")
    print(acc1)


# function_4()

def function_5():
    import time
    print(f"Current Time : {time.time()}")
    print("Going for sleep ")
    time.sleep(2)
    print("Waking up from sleep")

# function_5()

def function_6():
    import os
    # print(f"removing sample.txt")
    # os.remove("sample.txt")
    print(f"Name of os : {os.name}")
    print(f"Cpu count : {os.cpu_count()}")
    print(f"Current dir : {os.curdir}")

function_6()