import numpy as np

# Mathematical opertions on numpy
def function_1():
    arr1 = np.array([10, 20, 30, 50])
    # In below statement 20 will be added in every ele of an array
    # We are performing mathemtical operation on numpy array which is called as "Broadcasting"
    # As it is performed on every ele.
    print(f"arr1 + 20 : {arr1 + 20}")
    print(f"arr1 - 20 : {arr1 - 20}")
    print(f"arr1 * 20 : {arr1 * 20}")
    print(f"arr1 / 20 : {arr1 / 20}")



# function_1()


def test():
    arr1 = np.array([10, 20, 40, 50])
    # arr1.reshape(2, 2) doest not changes core arr1 in memory rather it will return new array so we
    # have to store it into some variable
    arr2 = arr1.reshape(2, 2)
    print(arr2)

# test()


def function_2():
    arr1 = np.array([10, 20, 40, 50])
    print(f"arr1 < 20 : {arr1 < 20}")
    print(f"arr1 > 20 : {arr1 > 20}")
    print(f"arr1 == 20 : {arr1 == 20}")


# function_2()

def function_3():
    arr1 = np.array([10, 20, 40, 50])
    arr2 = np.array([10, 20, 40, 50])
    print(f"arr1 + arr2 : {arr1 + arr2}")
    print(f"arr1 - arr2 : {arr1 - arr2}")
    print(f"arr1 * arr2 : {arr1 * arr2}")


function_3()
