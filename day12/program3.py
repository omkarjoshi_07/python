class Person:
    def __init__(self, name, address, age):
        self.__name = name
        self.__address = address
        self.__age = age

    def print_person_details(self):
        print(f"Name : {self.__name}")
        print(f"Address : {self.__address}")
        print(f"Age : {self.__age}")


class Employee(Person):
    def __init__(self, ID, company, name, age , address):
        Person.__init__(self, name, age, address)
        self.__ID = ID
        self.__company = company

    def print_details(self):
        self.print_person_details()
        print(f"Employee ID : {self.__ID}")
        print(f"Company : {self.__company}")


# Student is derived from Person
class Student(Person):

    def __init__(self, roll_no, name, age, address):
        Person.__init__(self, name, age, address)
        self.__roll_no = roll_no


emp_1 = Employee(101, "Infosys", "xyz", 26, "Pune")
emp_1.print_details()



# p1 = Person("xyz", "Kolhapur", 25)
# print(p1.__dict__)

# s1 = Student(25)
# print(s1.__dict__)


# class Car:
#     pass
#
# class Alto(Car):
#     pass
#
#
# alto = Alto()
# print(alto.__dict__)


