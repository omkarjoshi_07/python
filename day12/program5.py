# Multilevel Inheritance

class Person:
    def __init__(self, name):
        self._name = name

    def print_person_details(self):
        print(f"Name : {self._name}")


class Employee(Person):
    def __init__(self, name, id):
        Person.__init__(self, name)
        self._id = id

    def print_employee_details(self):
        print(f"ID : {self._id}")


class Manager(Employee):
    def __init__(self, name, id, department):
        Employee.__init__(self, name, id)
        self.__department = department

    def print_manager_details(self):
        self.print_person_details()
        self.print_employee_details()
        print(f"Department : {self.__department}")


manager = Manager("xyz", 101, "sales")
manager.print_manager_details()
