class Employee:
    def __init__(self, id, name, email, salary):
        self.__empid = id
        self.__name = name
        self.__email = email
        self.__salary = salary

    def print_employee_details(self):
        # print(f"Name : {self.__name}")
        # print(f"email : {self.__email}")
        # print(f"Salary : {self.__salary}")
        print(f"| {self.__empid:^3} | {self.__name:<15} | {self.__email:<15} | {self.__salary:^6} | ")


class Company:
    def __init__(self, name, address):
        self.__name = name
        self.__address = address
        self.__employees = []

    def add_employee(self, name, email, salary):
        id = len(self.__employees) + 1
        new_employee = Employee(id, name, email, salary)
        self.__employees.append(new_employee)

    def print_company_details(self):
        print(f"Company Name : {self.__name}")
        print(f"Company Address : {self.__address}")
        print("Employee Details  : ")
        for employee in self.__employees:
            employee.print_employee_details()
            # print("*" * 50)


c1 = Company("Infosys", "Hinjewadi, Phase III, Pune")
c1.add_employee("xyz", "xyz@gmail.com", 20000.00)
c1.add_employee("abc", "abc@gmail.com", 23000.00)
c1.add_employee("kdjf", "asd@gmail.com", 20000.00)
c1.print_company_details()
