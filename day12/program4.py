# single / simple inheritance

class Car:
    def __init__(self, car_type, cc):
        # protected members
        self._car_type = type
        self._cc = cc

    def print_car_details(self):
        print(f"Type of car : {self._car_type}")
        print(f"CC : {self._cc}")


class Alto(Car):
    def __init__(self, car_type, cc, model, version, price):
        Car.__init__(self, car_type, cc)
        self.__model = model
        self.__version = version
        self.__price = price

    def print_details(self):
        print(f"Model : {self.__model}")
        print(f"Version : {self.__version}")
        print(f"Price : {self.__price}")
        print(f"Car cc : {self._cc}")


a_1 = Alto("4 wheeler", 1200.00, "LXI", 2020, 450000.00)
a_1.print_details()

# a1 = Alto("VLXI", 2020, 450000.00)
# a1.print_car_details()

class Person:
    def __init__(self, name, address, age):
        self.__name = name
        self.__address = address
        self.__age = age

    def print_details(self):
        print(f"Name : {self.__name}")
        print(f"Address : {self.__address}")
        print(f"Age : {self.__age}")


class Employee(Person):
    def __init__(self, ID, company, name, age, address):
        Person.__init__(self, name, age, address)
        self.__ID = ID
        self.__company = company

    def print_details(self):
        # self.print_person_details()
        print(f"Employee ID : {self.__ID}")
        print(f"Company : {self.__company}")


# emp_1 = Employee(102, "BMC", "xyz", 26, "Kolhapur")
# emp_1.print_details()

