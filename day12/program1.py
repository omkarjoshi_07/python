# Association

class Employee:
    def __init__(self, name, email, salary):
        self.__name = name
        self.__email = email
        self.__salary = salary

    def print_employee(self):
        print(f"Name : {self.__name}")
        print(f"Email : {self.__email}")
        print(f"Salary : {self.__salary}")


class Company:
    def __init__(self, name, address):
        self.__name = name
        self.__address = address
        self.__employee = None
        print(self.__employee)

    def print_company_details(self):
        print(f"Company Name : {self.__name}")
        print(f"Company Address : {self.__address}")
        print(f"Employee Details : ")
        self.__employee.print_employee()

    def set_employee(self, name, email, salary):
        self.__employee = Employee(name, email, salary)
        print(self.__employee)


company = Company("TCS", "Hinjewadi phase III, Pune")
company.set_employee("xyz", "xyz@gmail.com", 20000.00)
company.print_company_details()
