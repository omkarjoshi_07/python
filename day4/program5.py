# list collection patterns


def list_operations_1():
    company_list = ["wurth-IT", "Capegemini", "Xcaliber", "Infosys", "Amdocs", "Capegemini", "Xcaliber"]
    # print(f"Companies: {company_list}")
    # company_list.remove("Capegemini")
    # print(f"Comapnies : {company_list}")

    posistions = list(range(company_list.count("Capegemini")))
    for i in posistions:
        company_list.remove("Capegemini")

    print(f"Companies: {company_list}")


# list_operations_1()


def list_operations_2():
    numbers = [10, 20, 30, 40, 50, 30, 30, 60, 70, 30]
    print(numbers)

    print(f"30 is present at {numbers.index(30, 0)}")
    print(f"30 is present at {numbers.index(30, 3)}")
    print(f"30 is present at {numbers.index(30, 6)}")
    print(f"30 is present at {numbers.index(30, 7)}")


# list_operations_2()


def list_operation_3():
    company_list = ["wurth-IT", "Capegemini", "Xcaliber", "Infosys", "Amdocs", "Capegemini", "Xcaliber"]
    # company_list_2 = company_list

    company_list_2 = company_list.copy()
    company_list_2.remove("wurth-IT")
    company_list_2.clear()
    print(f"Companies : {company_list}")
    print(f"Companies : {company_list_2}")


# list_operation_3()


def list_operation_4():
    company_list = ["Xcaliber", "Infosys", "Capegemini", "TCS"]
    print(f"No of Companies : {len(company_list)}")
    company_list.append("IBM")
    print(f"No of Companies : {len(company_list)}")
    print(f"Companies : {company_list}")
    company_list.append([10, 20, 30])
    print(f"Companies : {company_list}")
    print(f"No of Companies : {len(company_list)}")


# list_operation_4()


def list_indexing():
    numbers = [10, 20, 40, 100, 400]
    positions = range(len(numbers))

    for i in positions:
        print(f"numbers[{-(i + 1)}] = {numbers[-(i + 1)]}")


list_indexing()