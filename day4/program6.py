# problem -> fruits is list having fruits in it. We are required to take out only specific fruits
# depending upon user input and add them inside the basket.
# fruits -> lis of fruits
# basket -> list of fruits chosen by user
fruits = ["apple", "banana", "orange", "watermelon", "grapes", "kiwis", "mango"]


def print_fruits():
    # print(f"Fruits Available : {fruits}")
    for f in fruits:
        print(f"**** {f} ****")


def put_fruit_in_basket():
    print(f"Enter how many fruits you want to put into the basket")
    no_of_fruits = int(input("No of fruits : "))
    if no_of_fruits > len(fruits):
        print(f"Warning: Only {len(fruits)} fruits are available..Please enter valid number..")
    else:
        fruits_to_put = []
        positions = range(no_of_fruits)
        # print(len(positions))
        for index in positions:
            fruits_to_put.append(input("fruit : "))
        # print(fruits.index(fruits_to_put[0]))
        # print(f"fruits to be put : {fruits_to_put}")
        fruit_indexes = []
        for fruit in fruits_to_put:
            fruit_indexes.append(fruits.index(fruit))
        basket = []
        for put_fruit in fruit_indexes:
            basket.append(fruits[put_fruit])

        # print(f"Indexes of fruits to be put : {fruit_indexes}")
        print(f"Fruits in the basket : {basket}")
        # print(no_of_fruits)


print_fruits()
put_fruit_in_basket()
