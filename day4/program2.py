# indexing list


def function_1():
    numbers = [10, 20, 30, 40, 50]
    print(numbers)

    # positive indexing
    print(f"numbers[0] = {numbers[0]}")
    print(f"numbers[1] = {numbers[1]}")
    print(f"numbers[2] = {numbers[2]}")

# function_1()

def function_2():
    numbers = [10, 20, 30, 40, 50]
    print(numbers)

    # negative indexing
    print(f"numbers[-1] = {numbers[-1]}")
    print(f"numbers[-2] = {numbers[-2]}")
    print(f"numbers[-5] = {numbers[-5]}")

    positions = range(len(numbers))
    for i in positions:
        print(f"numbers[{-(i+1)}] = {numbers[-(i+1)]}")
        # print(i)


function_2()
