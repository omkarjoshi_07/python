def function_1():
    numbers = [10, 20, 30, 40, 50]
    print(numbers)

    numbers.remove(10) # remove 1st occurence
    print(numbers)

    countries = list(["india", "UK", "USA", "china", "japan"])
    print(countries)

    countries.remove("china")
    print(countries)

    numbers_2 = [10, 20, 30, 40, 30, 50, 20]
    print(numbers_2)

    numbers_2.remove(30)
    print(numbers_2)

    numbers_2.remove(30)
    print(numbers_2)

# function_1()


def function_2():
    positions = range(0, 11, 1)
    print(f"positions = {positions}, type = {type(positions)}")

    # for loop
    for i in positions:
        print(f"index = {i}")

    list_1 = list(positions)
    print(f"list_1 = {list_1}, type = {type(list_1)}")


# function_2()


def function_3():
    numbers = [10, 20, 30, 40, 50, 30, 30, 60, 70, 30]
    print(numbers)

    # count of 30
    print(f"30 is repeated {numbers.count(30)} times")

    positions = range(numbers.count(30))
    # deleteing all 30 occurences
    for index in positions:
        print("deleting 30")
        numbers.remove(30)

    print(numbers)
# function_3()

def function_4():
    numbers = [10, 20, 30, 40, 50, 30, 30, 60, 70, 30]
    print(numbers)

    print(f"30 is present at {numbers.index(30,0)}")
    print(f"30 is present at {numbers.index(30,3)}")
    print(f"30 is present at {numbers.index(30,6)}")
    print(f"30 is present at {numbers.index(30,7)}")


# function_4()

def function_5():
    numbers = [10, 20, 30, 40, 50, 30, 30, 60, 70, 30]
    print(numbers)

    # reverse list
    numbers.reverse()
    print(numbers)


# function_5()

def function_6():
    numbers = [10, 20, 30, 40, 50, 30, 30, 60, 70, 30]
    print(numbers)

    # numbers.sort()
    # print(numbers)

    #sort in desc order
    numbers.sort(reverse=True)
    print(numbers)



# function_6()

def function_7():
    numbers_1 = [20, 70, 10, 30, 20, 30, 30, 10, 100, 10]
    print(f"numbers_1 = {numbers_1}")

    # numbers_2 = numbers_1
    # numbers_2.remove(100)
    # print(f"numbes_1 = {numbers_1}")

    numbers_2 = numbers_1.copy()
    numbers_2.clear()
    print(f"numbers_1 = {numbers_1}")
    print(f"numbers_2 = {numbers_2}")


# function_7()

def function_8():
    numbers = [10, 20, 30, 40, 50]
    print(f"length of numbers = {len(numbers)}")

    # numbers.append(60)
    # print(f"length of numbers = {len(numbers)}")

    # 3 values added at one index
    # numbers.append([70,80,90])
    # print(f"length of numbers = {len(numbers)}")


    numbers.extend([70, 80, 90])
    print(f"length of numbers = {len(numbers)}")
    print(numbers)


# function_8()


def function_9():
    numbers = [10, 20, 30, 40, 50]
    print(f"numbers = {numbers}, len = {len(numbers)}")

    numbers[2] = 60
    print(f"numbers = {numbers}, len = {len(numbers)}")

# function_9()



