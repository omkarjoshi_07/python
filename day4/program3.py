# slicing -> Taking out sequential portion of collection

def function_1():
    """
        Appending elements at different indexes
        from one list into another.
    """
    # list of numbers
    numbers = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]

    list_1 = []
    positions = [2, 3, 4, 5, 6]
    for index in positions:
        list_1.append(numbers[index])

    print(f"list_1 = {list_1}")


# function_1()


def function_2():
    numbers = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
    print(f"numbers from 2 to 6 = {numbers[2:7]}")

    print(f"1st 5 elements from numbers = {numbers[0:6]}")

    print(f"numers from 0 to 6 = {numbers[:6]}")

    print(f"numers from 6 to end = {numbers[6:]}")

    print(f"0 to 6 = {numbers[0:6]}")

    print(f"all = {numbers[:]}")


# function_2()


def function_3():
    numbers = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]

    print(f"numbers from 1 to 7 with 2 step = {numbers[1:7:2]}")

    print(f"numbers from even index = {numbers[::2]}")

    print(f"numbers from odd index = {numbers[1::2]}")


# function_3()


def function_4():
    """
    print elements from the list
    which are at even and odd index resp.
    """
    numbers_list = [ 34, 56, 33, 77, 89, 90, 19, 43, 65]
    indexes = list(range(0, len(numbers_list), 2))
    # print(indexes)
    for index in indexes:
        print(f"ele at {index} = {numbers_list[index]}")


function_4()


# print(function_4.__doc__)