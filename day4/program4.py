#tuples

def function_1():
    #empty lists
    list_1 = []
    print(f"list_1 = {list_1}, type = {type(list_1)}")

    list_2 = list()
    print(f"list_2 = {list_2}, type = {type(list_2)}")

    #empty tuples
    tuple_1 = ()
    print(f"tuple_1 = {tuple_1}, type = {type(tuple_1)}")

    tuple_2 = tuple()
    print(f"tuple_2 = {tuple_2}, type = {type(tuple_2)}")



# function_1()

def function_2():
    numbers_1 = [10, 20, 30]
    print(f"numbers_1 = {numbers_1}")
    numbers_1.append(50)
    print(f"numbers_1 = {numbers_1}")


    #tuple is immutable
    tuple_1 = (10, 20, 30, 40)
    #we can't modify tuple once created
    # tuple_1.append(90) #not ok

    print(f"tuple_1 = {tuple_1[0:3:1]}")


function_2()
