#deinitializer

class Car:
    def __init__(self, model, price):
        self.__model = model
        self.__price = price
        print(f"{self.__model} is in Initialzer")

    #deinitializer
    #deinitializer contains logic that frees memory which we want to delete before object is destroyed
    def __del__(self):
        print(f"{self.__model} is in de-initializer")

    def print_car_details(self):
        print(f"Model : {self.__model}")
        print(f"Price : {self.__price}")




c1 = Car("i20", 7.5)
c1.print_car_details()

#purpose of del is to delete object from memory we it is no longer required afer its execution
del c1

c3 = Car("alto", 4.5)

c2 = Car("Nano", 3.5)
c2.print_car_details()

c3.print_car_details()