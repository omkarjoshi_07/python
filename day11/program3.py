#initializer, getters, setters, facilitators

class Mobile:
    """
    this is mobile class having attributes
    brand, price, model
    """
    # initializer having private data members
    def __init__(self, brand, model, price):
        self.__brand = brand
        self.__model = model
        self.__price = price

    # facilitators
    def print_mobile_details(self):
        print(f"Brand : {self.__brand}")
        print(f"Model : {self.__model}")
        print(f"Price : {self.__price}")
        print('-' * 20)

    def is_affordable(self):
        if self.__price > 25000:
            print(f"{self.__model} is not affordable")
        else:
            print(f"{self.__model} is affordable")

    # setters
    def set_brand(self, brand):
        self.__brand = brand

    def set_model(self, model):
        self.__model = model

    def set_price(self, price):
        self.__price = price

    # getters
    def get_brand(self):
        return self.__brand

    def get_model(self):
        return self.__model

    def get_price(self):
        return self.__price


m1 = Mobile("iphone", "x11", 40000.00)
m2 = Mobile("xiomi", "redmi 4", 16000.00)
m3 = Mobile("iphone", "s2", 24000.00)


# m1.print_mobile_details()
# m1.is_affordable()
# m2.print_mobile_details()
# m2.is_affordable()
# m3.print_mobile_details()
# m3.is_affordable()

m1.set_price(35000.00)
# print(f"changed price of {m1.get_model()} is {m1.get_price()}")
# print(m1.__brand)
print(m1.__dict__)
m1.__brand = "ads"
print(m1.__dict__)