# use of initializer, facilitators, deinitializer, getters, setters

class Student:
    def __init__(self, name, address, age, roll_no, school, marks):
        self.__name = name
        self.__address = address
        self.__age = age
        self.__roll_no  = roll_no
        self.__school = school
        self.__marks = marks

    def print_student_details(self):
        print(f"Name : {self.__name}")
        print(f"Address : {self.__address}")
        print(f"Age : {self.__age}")
        print(f"Roll No : {self.__roll_no}")
        print(f"School : {self.__school}")
        print(f"Marks : {self.__marks}")
        print('-' * 20)

    def print_result(self):
        print(f"Grade of {self.__name} is..")
        if (self.__marks > 60) and (self.__marks < 80):
            print("Second class")
        elif self.__marks > 80:
            print("First class")
        else:
            print("Third class")
        print('*' * 20)


# s1 = Student("raj", "Pune", 25, 101, "Sunbeam", 50)
# s2 = Student("ram", "Pune", 24, 102, "Sunbeam", 70)
# s3 = Student("shyam", "Karad", 25, 103, "Sunbeam", 90)
#
#
# s1.print_student_details()
# s1.print_result()
#
# s2.print_student_details()
# s2.print_result()
#
# s3.print_student_details()
# s3.print_result()

def Enroll_students():
    st_list = []
    numbers = [0, 1]
    for i in numbers:
        name = input("Name: ")
        address = input("Address: ")
        age = int(input("Age:  "))
        roll_no = int(input("Roll No:  "))
        school = input("School:  ")
        Marks = float(input("Marks:  "))
        st_list.append(Student(name, address, age, roll_no, school, Marks))

    for i in numbers:
        st_list[i].print_student_details()
        st_list[i].print_result()


Enroll_students()





