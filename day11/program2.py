class Person:
    def __init__(self, name, address, age):
        self.name = name
        self.address = address
        if age > 0:
            self.age = age
        else:
            self.age = 0
            print("Please Enter the valid age...")

    def print_details(self):
        print(f"Name : {self.name}")
        print(f"Address : {self.address}")
        print(f"Age : {self.age}")
        print('-' * 20)

    def set_age(self, age):
        if age > 0:
            self.age = age
        else:
            self.age = 0
            print("Please Enter the valid age...")


p1 = Person("Shybaj", "Yadrav", 26)
print(p1.__dict__)
p1.print_details()

# age is public
# p1.age = -50
# p1.set_age(-50)
p1.print_details()

p1.name = "sm"
p1.address = "kop"
p1.print_details()
print(p1.__dict__)

# as data members are public we can access them outside the class using object directly
