class Person:
    def __init__(self, name, address, age):
        setattr(self, "name", name)
        setattr(self, "address", address)
        setattr(self, "age", age)

    def print_person_details(self):
        print(f"Name : {getattr(self, 'name')}")
        print(f"Address : {getattr(self, 'address')}")
        print(f"Address : {getattr(self, 'age')}")


p1 = Person("person1", "Pune", 25)
p1.print_person_details()

setattr(p1, 'age', -20)
p1.print_person_details()

