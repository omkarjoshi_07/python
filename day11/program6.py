class Bike:
    def __init__(self, make, model, price, milage, max_speed):
        self.__Make = make
        self.__Model = model
        self.__Price = price
        self.__Milage = milage
        self.__Max_Speed = max_speed

    def set_make(self, make):
        self.__Make = make

    def set_model(self, model):
        self.__Model = model

    def set_price(self, price):
        self.__Price = price

    def set_milage(self, milage):
        self.__Milage = milage

    def  set_max_speed(self, max_speed):
        self.__Max_Speed = max_speed

    def print_bike_details(self):
        print(f"Make : {self.__Make}")
        print(f"Model : {self.__Model}")
        print(f"Price : {self.__Price}")
        print(f"Milage : {self.__Milage}")
        print(f"Max Speed : {self.__Max_Speed}")

    def __del__(self):
        print("Inside the deinitialzer..")


bike_1 = Bike("Yamaha", "MT-15", 2820000.00, 50.00, 200.00)
print(bike_1.__dict__)

# below statement will create new variable with different name
bike_1.__price = 300000.00
print(bike_1.__dict__)

# bike_1.print_bike_details()

# deleting object when not needed
del bike_1