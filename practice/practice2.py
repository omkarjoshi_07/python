#add item in nested list

def function_1():
    list_1 = [10, 20, [300, 400, [5000, 6000], 500], 30, 40]
    item = int(input("Element to add: "))
    # print(f"item = {item}, type = {type(item)}")

    #add element after 6000
    # print(f"6000 is located at = {list_1[2][2].index(6000)}")
    list_1.insert(list_1[2][2].index(6000),item)
    print(f"list_1 after insertion = {list_1}")

# function_1()

num_1 = 100
def function_2():
    num_2 = 200
    print(f"num_1 = {num_1}, num_2 = {num_2}")

    global num_1
    num_1 = 300
    print(f"num_1 = {num_1}")

# function_2()