def reverse_list():
    list_1 = [400, 300, 500, 100, 150]
    list_1.reverse()
    print(list_1)
# reverse_list()


#concatnate two lists index wise
def concat_list():
    list_1 = ["m", "na", "i", "ke"]
    list_2 = ["y", "me", "s", "lly"]

    result_list = []
    positions = list(range(0,len(list_1)))
    for index in positions:
        result_list.append((list_1[index]+list_2[index]))
    print(result_list)

# concat_list()

#calclulate sqaure of each element
square_lambda = lambda num : num ** 2

def calculate():
    list_1 = [1, 2, 3, 4, 5, 6, 7]
    result = list(map(square_lambda,list_1))

    print(f"squares of list = {result}")

# calculate()


#remove empty string from existing string
def remove_empty():
    strings = ["Mike", "", "Emma", "", "Kelly", "", "Brad"]
    # positions = range(0, len(strings))
    # for i in positions:
    #     if None in strings:
    #         strings.remove(None)
    for s in strings:
        if s == "":
            strings.remove(s)

    print(strings)
# remove_empty()

