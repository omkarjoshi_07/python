# tuples

def function_1():
    list_1 = [10]
    print(f"list_1 = {list_1}, type = {type(list_1)}")

    list_2 = ["manchester united"]
    print(f"list_2 = {list_2}, type = {type(list_2)}")

    # considering 10  as int in round bracket
    tuple_1 = (10)
    print(f"tuple_1 = {tuple_1}, type = {type(tuple_1)}")

    # tuple
    tuple_2 = (10,)
    print(f"tuple_2 = {tuple_2}, type = {type(tuple_2)}")

    tuple_3 = "Real Madrid"
    print(f"tuple_3 = {tuple_3}, type = {type(tuple_3)}")

    tuple_4 = ("Wolves",)
    print(f"tuple_4 = {tuple_4}, type = {type(tuple_4)}")


# function_1()


def function_2():
    numbers = (10, 20, 30, 20, 40, 30, 20)
    print(f"numbers = {numbers}, type = {type(numbers)}")

    print(f"occurrence of 20 in numbers = {numbers.count(20)}")
    print(f"index of 20 in numbers = {numbers.index(20)}")
    print(f"index of 20 in numbers after 1st index = {numbers.index(20,2)}")


function_2()
