def set_operation_1():
    fruits = {}
    print(f"{type(fruits)}") # dict

    # insertion order is not maintained
    countries = {"india", "England", "Australia", "Japan", "india"}
    print(f"Countries = {countries}")

    duplicate_countries = countries.copy()
    print(f"Duplicate Countries = {duplicate_countries}")

    duplicate_countries.add("USA")
    print(f"Duplicate Countries = {duplicate_countries}")


set_operation_1()
