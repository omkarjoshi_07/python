# dictionary
def function_1():
    # empty dictonary
    dict_1 = {}
    print(f"dict_1 = {dict_1}, type = {type(dict_1)}")

    person = {"name": "david", "address": "mumbai", "age": 30}
    print(f"person = {person}, type = {type(person)}")

    employee = {"name": "alex", "empid": 1001, "department": "design", "salary": 25000.00}
    print(f"employee = {employee}, type = {type(employee)}")

    print(f"name of employee = {employee['name']}")

    print(f"keys in employee = {employee.keys()}")
    print(f"values in employee = {employee.values()}")


# function_1()


def function_2():
    person = {
        "name": "alex",
        "age": 30,
        "address": "Pune",
        "companies": (
            "apple", "microsoft", "Pixar"
        )
    }
    print(f"companies = {person['companies']}")
    print(f"companies = {person.get('companies')}")

    # email is not there, so app will crash
    # print(f"Email = {person['email']}")

    # app will not crash if we use get() function, as it gives None value
    print(f"Email = {person.get('email')}")


# function_2()


def function_3():
    employee = {"name": "alex", "empid": 1001, "department": "design", "salary": 25000.00, 1: "test value"}

    # prints test value as 1 is key present in employee
    print(f"employee[1] = {employee[1]}")
    keys = employee.keys()
    for k in keys:
        print(f"key = {k}")
    print(f"employee keys = {employee.keys()}")
    print(f"employee values = {employee.values()}")

    popped_ele = employee.pop("empid")
    print(f"popped ele = {popped_ele}")

    # prints key and values
    print(employee.items())


# function_3()