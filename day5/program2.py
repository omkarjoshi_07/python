def function_1():
    # tuple having only one element -> num = [100,]
    # numbers = (10, 20)
    numbers = 10, 20
    print(f"numbers = {numbers}, type = {type(numbers)}")
    print(f"numbers[0] = {numbers[0]}")
    print(f"numbers[1] = {numbers[1]}")

    n1 = 100
    n2 = 200
    n1, n2 = 10, 20  # tuple behind the scene -> (n1, n2) = (10, 20) and can be decoupled.
    print(f"n1 = {n1}, type = {type(n1)}")
    print(f"type of n1 and n2 = {type((n1, n2))}")


# function_1()


def function_2():
    # internally -> (n1, n2) = (10, 20)-> (n1, n2) -> treated as tuple
    n1, n2 = 10, 20
    n2, n1 = n1, n2
    print(f"after swapping n1 = {n1}, n2 = {n2}, type of (n1, n2) = {type((n1, n2))}")
    print(f"count of 10 in (n1, n2) tuple = {(n1, n2).count(10)}")


# function_2()


# multiple return values


def math_operation(num1, num2):
    addition = num1 + num2
    substraction = num1 - num2
    multiplication = num1 * num2
    division = num1 / num2

    return addition, substraction, multiplication, division


addition_res, substraction_res, multiplication_res, division_res = math_operation(5, 4)
print(f"addition = {addition_res}, type of addition = {type(addition_res)}")
print(f"substraction = {substraction_res}")
print(f"multiplication = {multiplication_res}")
print(f"division = {division_res}")
