# set collection
def function_1():
    # list
    list_1 = [10, 20, 30, 40, 50, 10, 20, 30, 40, 50, 10, 20, 30, 40, 50]
    print(f"list_1 = {list_1}, type = {type(list_1)}")

    # tuple
    tuple_1 = (10, 20, 30, 40, 50, 10, 20, 30, 40, 50, 10, 20, 30, 40, 50)
    print(f"tuple_1 = {tuple_1}, type = {type(tuple_1)}")

    set_1 = {10, 20, 30, 40, 50, 10, 20, 30, 40, 50, 10, 20, 30, 40, 50}
    print(f"set_1 = {set_1}, type = {type(set_1)}")

    # picks only unqiue elements.
    technologies = {"Java", ".Net", "Mean Stack", "Database", "Java", ".Net"}
    print(f"Technologies = {technologies}")

    num = {1000}
    print(f"type of num = {type(num)}")


# function_1()


def function_2():
    # empty list
    list_1 = []
    list_2 = list()

    # empty tuple
    tuple_1 = ()
    tuple_1 = tuple()

    # creates empty dictionary
    set_1 = {}
    print(f"set_1 = {set_1}, type = {type(set_1)}")

    # to create empty set
    set_2 = set()
    print(f"set_2 = {set_2}, type = {type(set_2)}")


# function_2()


def function_3():
    s1 = {10, 20, 30, 40}
    s2 = {30, 40, 50, 60}
    print(f"s1 = {s1}, s2 = {s2}")

    print("-"*50)

    print(f"s1 intersection s2 = {s1.intersection(s2)}")
    print(f"s2 intersection s1 = {s2.intersection(s1)}")

    print("-"*50)

    print(f"s1 union s2 = {s1.union(s2)}")

    print("-"*50)

    print(f"s1 - s2 ={s1.difference(s2)}")
    print(f"s2 - s1 ={s2.difference(s1)}")


# function_3()


def function_4():
    ages = [20, 23, 20, 21, 24, 24, 23, 20, 21, 22, 25]
    print(f"Unique ages = {set(ages)}")

    # remove duplicacy from list
    cities = ["Pune", "Mumbai", "Delhi", "Kolhapur", "Goa", "Pune", "Mumbai"]
    print(f"Unique cities = {set(cities)}")

    # tuple cannot be passed to set to extract the unique elements.
    mobile_brands = ("samsung", "Xiomi", "Oppo", "Vivo", "Samsung", "vivo")
    print(f"Unique brands = {set(mobile_brands)}")


# function_4()


def function_5():
    s1 = {1, 2, 3, 4, 5}
    print(s1)

    s1.add(20)
    print(s1)

    list_1 = list(s1)
    list_1.sort()
    print(f"sorted set = {list_1}")


# function_5()


def function_6():
    #immutable set
    # s1 = frozenset([10, 20, 30, 40])
    s1 = frozenset({10, 20, 30, 40, 50})
    print(f"s1 = {s1}, type = {type(s1)}")

    # not ok as frozenset is immutable
    # s1.add(20)


# function_6()


def function_7():
    employee_salaries = {20000.00, 30000.00, 50000.00, 10000.00, 40000.00}
    sorted_salaries = list(employee_salaries)
    sorted_salaries.sort()
    print(f"Sorted Employee salaries = {sorted_salaries} ")


function_7()