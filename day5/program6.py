def function_1():
    employee = {"Name": "xyz", "Employee_ID": "100", "Salary": 25000.00, "Address": "Pune", "Company": "sdhfd"}
    print(f"Employee_Details : {employee}, type of employee = {type(employee)}")
    print(f"{employee['Name']}")
    print(f"employee keys = {employee.keys()}")
    print(f"employee values = {employee.values()}")
    tuple_1 = (100, 200)
    print(f"{tuple_1[0]}")

    list_1 = [1000, 2000]
    print(f"{list_1[0]}")

    # print(employee['email'])
    print(f"Employee Email : {employee.get('email')}")
    # get method wil return None if not found thereby avoiding crash of application


# function_1()


def function_2():
    employee = {"Name": "xyz", "Employee_ID": "100", "Salary": 25000.00, "Address": "Pune", "Company": "sdhfd"}
    employee_keys = employee.keys()
    employee_values = employee.values()
    print(f"Type of employee_keys = {type(employee_keys)}")
    print(f"Type of employee_values = {type(employee_values)}")

    for k in employee_keys:
        print(f"Key : {k}")

    for v in employee_values:
        print(f"value : {v}")

    popped_ele = employee.pop("Address")
    print(f"Popped element : {popped_ele}")

    print(f"employee Dict = {employee.items()}")


function_2()
