from flask import Flask, render_template, jsonify, request, make_response

app = Flask(__name__)


@app.route('/', methods=(['GET']))
def hello():
    answer = "Hello"
    return render_template('index.html', answer=answer)


app.run(host='localhost', port=5000, debug=True)
