from flask import Flask, render_template, request, make_response, jsonify
import mysql.connector

app = Flask(__name__)


@app.route("/employees", methods=["GET"])
def get_employees():
    connection = mysql.connector.connect(host="localhost", port=3306, user="root", password="new-password",
                                         database="python_test")
    cursor = connection.cursor()
    query = f"select * from employee;"
    cursor.execute(query)
    employees = cursor.fetchall()

    list_emps = []
    for emp in employees:
        list_emps.append({
            "EmpID": emp[0],
            "Name": emp[1],
            "Department": emp[2],
            "Salary": emp[3],
            "Location": emp[4]
        })
    cursor.close()
    connection.close()

    return render_template("employees.html", employees=list_emps)


app.run(host="localhost", port=5000, debug=True)
