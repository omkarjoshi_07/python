# import beauiful soup
from bs4 import BeautifulSoup


def function_1():
    html = """
                <html>
                    <body>
                        <h1>Welcome to Home Page</h1>
                    </body>
                </html>
    """

    # print(html)

    # create soup
    soup = BeautifulSoup(html, 'html.parser')

    # find the h1 ele
    h1 = soup.find('h1')
    print(h1.text)


function_1()


def function_2():
    pass