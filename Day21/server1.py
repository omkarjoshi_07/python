from flask import Flask, jsonify, request, make_response

app = Flask(__name__)


@app.route("/", methods=["GET"])
def root():
    return '<h1 style="color: red; font-family: arial; text-align: center">Welcome to application </h1>'


app.run(host="localhost", port=4000, debug=True)
