
def check_vote(name, age):
    """
    this function is checking voting eligibility
    :param name :name of person
    :param age: age of person
    :return nothing
    """
    if age >= 20:
        print(f"{name} is eligible for voting")
    else:
        print(f"{name} is not eligible for voting")


check_vote("person1", 18)
check_vote("person2", 24)

# print(check_vote()) #must give arguments