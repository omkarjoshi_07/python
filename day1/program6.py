# return type functions
# by-default every function return None


def add_1(num1, num2):
    """
    this is going to add two numbers
    """
    return num1 + num2

# result = add_1(10,5)
# print(f"Addition is = {result}")


# print(f"Addition is = {add_1(5, 4)}")


# def divide(n1,n2): #returns None
#     print(n1+n2)
# print(divide(4,2))


def non_returning_function():
    """
    this function has no return statement, but it returns None
    """
    pass


# print(type(non_returning_function()))
# print((non_returning_function())


def function_3():
    def function_4():
        return "function_4"

    return function_4()


print(f"result = {function_3()}")

