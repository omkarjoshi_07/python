#int
num = 100
print(f"num = {num}, type of num = {type(num)}")
#float
num2 = 100.3
print(f"num2 = {num2}, type of num2 = {type(num2)}")
#string #str
name = 'omkar'
print(f"name = {name}, type of name = {type(name)}")
#str
department = "sales"
print(f"department = {department}, type of department = {type(department)}")

"""
this is comment
"""
#multiline string
address = """32,street
ganhesh nagar,
kothrud,
pune"""
print(f"address = {address}, type of address = {type(address)}")

#bool
flag = False
print(f"flag = {flag}, type of flag = {type(flag)}")
