# functions


# empty function
def function_1():
    """
    this is just empty function
    this should have aleast one statement to compensate for indentation
    """
    pass


function_1()
# print doc string
# print(f" doc string for function_1: {function_1.__doc__}")


# parameterless function
def function_2():
    """
    this is test function 2
    this is not taking any parameters
    """
    print("inside function 2")


# function_2()
# print(f"docstring for function_2 = {function_2.__doc__}")
# print(f"docstring for function_1 = {function_1.__doc__}")
# docstring of print function
# print(print.__doc__)


# parametrized function
def function_3(param):
    """
    this is paramterized function
    which is taking argument
    """
    print(f"inside function 3")
    print(f"param = {param}, type of param = {type(param)}")


# function_3(10)  # ok int
# function_3(1.54)  # ok float
# function_3("hello")  # ok str
# function_3(None)  # ok None
# function_3(True)
# function_3(1245435354.45367676765574355) # float
# 'None' keyword is used to represent null value or no value.


def function_4(param):
    print(f"inside function 4")
    print(f"{param} is passed, type = {type(param)}")


# function_4(print) # built in function is passed
function_4(function_3(function_2()))


# def function_5():
    # pass

# function_5(109)

