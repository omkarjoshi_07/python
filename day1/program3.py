# control statements

name1 = "person1"
age1 = 15
if age1 >= 20:
    print(f"{name1} is eligible for voting")
    print(f"{name1} can vote now")
else:
    print(f"{name1} is not eligible for voting")
    print(f"{name1} cannot vote now")

name2 = "person2"
age2 = 25
if age2 >= 20:
    print(f"{name2} is eligible for voting")
    print(f"{name2} can vote now")
else:
    print(f"{name2} is not eligible for voting")
    print(f"{name2} cannot vote now")

def f1():
    pass
    print('helllo')
    pass
f1()
