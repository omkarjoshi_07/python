def function_1():
    """
    finds square, cube of numbers of collection
    using map/filter and lambda
    """
    numbers = [2, 3, 4, 1, 5, 7]
    print(f"numbers = {numbers}")

     # calculate square
    squares = list(map(lambda num: num ** 2, numbers))
    print(f"Squares = {squares}")

    # calculate cube
    cubes = list(map(lambda  num: num ** 3, numbers))
    print(f"Cubes = {cubes}")


# function_1()

def function_2():
    """
    prints even and odd nos from collection
    """
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 12]
    print(f"numbers = {numbers}")

    # even nos
    even_nos = list(filter(lambda num : num % 2 == 0, numbers))
    print(f"even numbers = {even_nos}")

    # odd nos
    odd_nos = list(filter(lambda num: num % 2 != 0, numbers))
    print(f"odd numbers = {odd_nos}")


# function_2()


def function_3():
    """
    find out squares of even numbers
    """
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 12]
    even_nos = list(filter(lambda n: n % 2 == 0, numbers))

    squares_even = list(map(lambda n: n ** 2, even_nos))
    print(f"Squares of even numbers = {squares_even}")

# function_3()


def function_4():
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 12]
    odds = filter(lambda n: n % 2 != 0, numbers)
    cube_odds = tuple(map(lambda n: n ** 3, odds))
    print(f"Cube of odd numbers = {cube_odds}")


# function_4()


def function_5():
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 12]
    nums = list(filter(lambda num: num > 5, numbers))
    print(nums)


function_5()
