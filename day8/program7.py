def function_1():
    """
    Create and write into file
    """
    file_1 = open("test.txt", "w")
    file_1.write("This is a test file")

    file_1.close()


# function_1()


def read_file():
    file_1 = open('test.txt', 'r')
    file_1_data = file_1.read()
    print(f"Data in test.txt : {file_1_data}")
    print(f"Pointer is pointing at {file_1.tell()}")


read_file()
