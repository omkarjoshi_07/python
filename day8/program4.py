# file handling

def function_1():
    """
    write file
    """
    file = open('test.txt', 'w')
    file.write("Welcome to file handling in python")

    file.close()


# function_1()


def function_2():
    file = open('test.txt', 'w')
    # write will overwrite existing content
    file.write("this is first line")
    file.close()

# function_2()


def function_3():
    file = open('test.txt', 'r')
    # data = file.read()

    # print(f"file content = {data}")

    # file pointer current position
    print(f"file pointer is pointing to {file.tell()}")

    # read next 4 bytes
    data = file.read(4)
    print(f"data = {data}")
    print(f"file pointer is pointing to {file.tell()}")

    data = file.read(4)
    print(f"data = {data}")
    print(f"file pointer is pointing to {file.tell()}")

    # set file pointer to 0
    file.seek(0)
    print(f"file pointer is pointing to {file.tell()}")

    data = file.read(10)
    print(f"data = {data}")
    file.close()


# function_3()


def function_4():
    file = open('test.txt', 'r')
    # line1 = file.readline()

    # print(f"line1 = {line1}")

    all_lines = file.readlines()
    # prints \n as it is
    print(f"all lines = {all_lines}")

    for line in all_lines:
        print(line.replace('\n', ''))

    file.close()


# function_4()


def function_5():
    file = open('test.txt', 'a')
    file.write("this is appended line")
    file.close()


function_5()
