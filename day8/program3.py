# list comprehension
def function_1():
    """
    4th power of numbers from collection
    """
    # 1st way
    numbers = [1, 4, 5, 2, 3]
    # 1st way
    temp_list = []
    for i in numbers:
        temp_list.append(i ** 4)
    print(f"4h power = {temp_list}")

    # 2nd way
    result = list(map(lambda n: n ** 4, numbers))
    print(f"4h power = {result}")


# function_1()


def function_2():
    """
    list comprehension
    """
    numbers = [1, 4, 5, 2, 3, 7, 8, 9]
    squares = [n ** 2 for n in numbers]
    print(f"squares = {squares}")


# function_2()


def function_3():
    """
    cubes using list comprehension
    """
    numbers = [1, 4, 5, 2, 3, 7, 8, 9]
    cubes = [n ** 3 for n in numbers]
    print(f"cubes = {cubes}")
    fourth_roots = [num ** 4 for num in numbers]
    print(f"Fourth roots = {fourth_roots}")


function_3()


def function_4():
    """even numbers from collection using filter"""

    numbers = [1, 4, 5, 2, 3, 7, 8, 9]
    evens = list(filter(lambda n : n % 2 == 0, numbers))
    print(f"even nos = {evens}")

# function_4()


def function_5():
    """
    even nos from collection using list comprehension
    """
    numbers = [1, 4, 5, 2, 3, 7, 8, 9]
    # even_nos = [n for n in filter(lambda n : n % 2 == 0, numbers)]
    even_nos = [n for n in numbers if n % 2 == 0]
    print(f"even nos = {even_nos}")

# function_5()


def function_6():
    """
    finds sqaure of even nos and cube of odd nos from collection
    """
    numbers = [1, 4, 5, 2, 3, 7, 8, 9]

    square_evens = [n ** 2 for n in numbers if n % 2 == 0]
    print(f"sqaure of even nos = {square_evens}")

    # cube of odds
    cube_odds = [n ** 3 for n in numbers if n % 2 != 0]
    print(f"cube of odds = {cube_odds}")

# function_6()


def function_7():
    """
    print number and its sqaure as a tuple using list comprehension
    """
    numbers = [1, 4, 5, 2, 3, 7, 8, 9]
    squares = list(map(lambda n : n ** 2, numbers))

    result = list(zip(numbers, squares))
    print(f"numbers and squares using zip = {result}")

    # list comprehension
    numbers_squares = [(n, n ** 2) for n in numbers]
    print(f"numbers and squares using list comprehension= {result}")


# function_7()

def function_8():
    """
    combine two lists using list comprehension
    """
    numbers_1 = [1, 2, 3, 4, 5]
    numbers_2 = [6, 7, 8, 9, 10]

    # using zip
    result1 = list(zip(numbers_1, numbers_2))
    print(result1)

    result2 = [(numbers_1[i], numbers_2[i]) for i in range(len(numbers_1))]
    print(result2)


# function_8()

