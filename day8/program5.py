def calculate_salary(emp):
    return emp['Salary'] + 10000


def update_salary():
    employees = [
        {"Empid": 101, "Name": "xyz", "Salary": 25000, "Company": "ad"},
        {"Empid": 102, "Name": "asd", "Salary": 35000, "Company": "dsdd"},
        {"Empid": 103, "Name": "ghjh", "Salary": 45000, "Company": "fgh"}

    ]
    updated_salaries = list(map(calculate_salary, employees))
    # print(updated_salaries)
    employees_with_new_sal = list(zip(employees, updated_salaries))
    print(employees_with_new_sal)


update_salary()
