def function_1():
    """
    prints numbers and their square using zip()
    """
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    squares = list(map(lambda n: n ** 2, numbers))
    print(f"squares = {squares}")

    number_and_squares = list(zip(numbers, squares))
    print(f"Numbers and Squares = {number_and_squares}")


# function_1()


def function_2():
    """
    merges two lists using zip
    """
    list_1 = [1, 4, 6, 8]
    list_2 = [3, 9, 10, 12]

    combined_list = list(zip(list_1, list_2))
    print(f"Combined List = {combined_list}")


# function_2()


def function_3():
    """
    merge three list together using zip
    """
    list_1 = [1, 4, 6, 8, 22]
    list_2 = [3, 9, 10, 12]
    list_3 = [5, 7, 2, 13, 15]

    # returns list of tuples in which, elements having same index in three lists will be in same tuple
    # no of elements must be same in all the lists.
    result = list(zip(list_1, list_2, list_3))
    print(f"result = {result}")


function_3()


def function_4():
    """
    combine strings using zip
    """

    str_1 = "welcome to python"
    str_2 = "guest"

    result = list(zip(str_1, str_2))
    print(f"result ={result}")


# function_4()
