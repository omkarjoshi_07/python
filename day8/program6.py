def calculate_squares():
    numbers = [10, 2, 5, 6, 7, 30]
    # 1 using for loop
    # squares = []
    # for num in numbers:
    #     squares.append(num ** 2)
    #
    # print(f"Squares = {squares}")

    # 2 using lambda
    # squares = list(map(lambda num: num ** 2, numbers))
    # print(f"Squares = {squares}")

    # 3 using list comprehension
    # squares = [num ** 2 for num in numbers]
    squares = [(num, num ** 2) for num in numbers]
    print(f"Squares = {squares}")

    # print even numbers as it is and if num is odd then square it and print it.
    print([num if num % 2 == 0 else num ** 2 for num in numbers])


# calculate_squares()


def check_evens():
    numbers = [2, 45, 65, 78, 90, 1]
    # using map and lambda
    # evens = list(filter(lambda n: n % 2 == 0, numbers))
    # print(f"EVen nos = {evens}")

    # evens = [n % 2 == 0 for n in numbers]
    # evens = [n for n in list(filter(lambda n: n % 2 == 0, numbers))]
    evens = [n for n in numbers if n % 2 == 0]
    print(f"Even nos = {evens}")


# check_evens()


def generate_numbers():
    """
    Generate numbers from 1 to 10 using list comprehension
    """
    print([n for n in range(1, 11)])


# generate_numbers()


def print_prime_nos():
    # print number from 1 to 50 which is divisible by 9
    res = [num for num in range(1, 51) if num % 3 == 0 if num % 9 == 0]
    print(res)


# print_prime_nos()


def check_prime():
    """
    This function checks each number in list for prime and print it using list comprehension
    """
    numbers = [2, 4, 6, 7, 3, 5]
    # flag = False
    # for n in numbers:
    #     for i in range(2, n):
    #         if n % i == 0:
    #             flag = True
    #     print(flag)
    #     flag = False
    # print(n for n in numbers for i in range(2, n) if n % i != 0)


check_prime()


def print_tables():
    """
    This function prints tables between 15 to 20 using list comprehension
    """
    print(f"{[[i * j for j in range(1, 11)] for i in range(15, 21)]}")


# print_tables()


def combine_list_ele():
    numbers = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    flattened_list = []
    for i in numbers:
        for n in i:
            flattened_list.append(n)

    # print(flattened_list)
    # print([n for i in numbers for n in i])

    matrix = [[1, 2], [3, 4], [5, 6], [7, 8]]
    print([[i[n] for i in matrix] for n in range(2)])


# combine_list_ele()


def make_pairs():
    """
    This function makes possible combination of pairs using list comprehension
    """
    chars = ["A", "B", "C", "D", "E"]
    # here (x, y) is giving me a tuple
    print([(x, y) for x in chars for y in chars if x != y])

    # here [x, y] is giving me a list
    print([[x, y] for x in chars for y in chars if x != y])


# make_pairs()


def paragraph_operate():
    para = """Example to check whether an integer is a prime number or not using for loop and 
           if...else statement. If the number is not prime, it's explained in output why it 
           is not a prime number.
           """
    # print(para.split(" "))
    # ordinary way to extract the word from paragraph and checking if length of each word is > 5
    # for word in para.split(" "):
    #     if len(word) > 5:
    #         print(word)

    # printing the words having length >  5 using list comprehension
    print([word for word in para.split(" ") if len(word) > 5])


# paragraph_operate()
