# operator overloading
class MyNumber:
    def __init__(self, num):
        self.__num = num

    def print_details(self):
        print(f"num = {self.__num}")

    def __add__(self, num2):
        print(self.__num + num2.__num)

    def __sub__(self, other):
        print(self.__num - other.__num)

    def __mul__(self, other):
        print(self.__num * other.__num)

    def __floordiv__(self, other):
        print(self.__num // other.__num)

    def __truediv__(self, other):
        print(self.__num / other.__num)

    def __pow__(self, power, modulo=None):
        print(self.__num ** power)

    def __gt__(self, other):
        print(self.__num > other.__num)

    def __lt__(self, other):
        print(self.__num < other.__num)

    def __ge__(self, other):
        print(self.__num >= other.__num)

    def __le__(self, other):
        print(self.__num <= other.__num)


num1 = MyNumber(100)
num2 = MyNumber(200)


def function_3():
    # addition = num1 + num2
    # num1.add(num2)

    n1 = 200
    n2 = 400

    # behind the scene -> int.__add__(n1, n2)
    # behind the scene -> n1.__add__(n2)
    # addition = n1 + n2
    # print(int.__dict__)
    #
    # str_1 = "string1"
    # str_2 = "string2"

    # behind the scene -> str.__add__(str_1, str_2)
    # behind the scene -> str_1.__add__(str2)
    # str3 = str_1 + str_2

    num1 + num2
    # print(f"Addition is  {addition}")

    num1 - num2
    num1 * num2
    num1 / num2
    num1 // num2
    num1 ** 2

    # MyNumber.__gt__(num1, num2)
    # num1 > num2
    # num1 < num2
    # num1 <= num2
    # num1 >= num2


# function_3()


