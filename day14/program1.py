# multiple inheritance

class Person:
    pass


class Employee(Person):
    pass


print(Person.__base__)
print(Employee.__base__)


class Faculty:
    def __init__(self, name, subject):
        self._name = name
        self._subject = subject

    def print_details(self):
        print(f"Name : {self._name}")


class LabAssistant:
    def __init__(self, name, lab):
        self._name = name
        self._lab = lab

    def print_details(self):
        print(f"Name : {self._name}")
        print(f"Lab : {self._lab}")


class FacultyLabAssistance(Faculty, LabAssistant):
    def __init__(self, name, subject, lab):
        Faculty.__init__(self, name, lab)
        LabAssistant.__init__(self, name, subject)

    def print_details(self):
        print(f"Name : {self._name}")
        print(f"Subject : {self._subject}")
        print(f"Lab : {self._lab}")


fc1 = FacultyLabAssistance("xyz", "Python", "Pyspark")
fc1.print_details()

# gives 1st class only i.e. Faculty
print(f"base class of FacultyLab Assistant : {FacultyLabAssistance.__base__}")


# gives 1st class both
print(f"base class of FacultyLab Assistant : {FacultyLabAssistance.__bases__}")


class Developer:
    pass


class Tester:
    pass


class DeveloperTester(Developer, Tester):
    pass


class Operations:
    pass


class DevOPs(Developer, Operations):
    pass
