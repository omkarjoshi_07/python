class Person:
    def __init__(self, name, age, address):
        self.__name = name
        self.__address = address
        self.__age = age
        print("Base class initializer")

    def print_person_details(self):
        print(f"Person's Name : {self.__name}")
        print(f"Person's Age : {self.__age}")
        print(f"Person's Address : {self.__address}")

    def __del__(self):
        print("Base class deinitializer")


class Student(Person):
    def __init__(self, name, age, address):
        Person.__init__(self, name, age, address)
        print("Derived class initializer")

    def __del__(self):
        print("Derived class deinitializer")


s1 = Student("xyz", 26, "Kolhapur")
# print(s1.__dict__)

# here we can call base class method using derived class reference
# s1.print_person_details()

