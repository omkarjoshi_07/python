class Animal:
    def __init__(self, name):
        self._name = name

    def make_sound(self):
        print(f"{self._name} makes no sound...")


class Lion(Animal):
    def __init__(self):
        Animal.__init__(self, "Lion")

    def make_sound(self):
        print(f"Lion roars..")


class Cow(Animal):
    def __init__(self):
        Animal.__init__(self, "Cow")

    def make_sound(self):
        print(f"Cow say moo moo..")


# c = Cow()
# c.make_sound()

class Elephant(Animal):
    def __init__(self):
        Animal.__init__(self, "Elephant")


# e = Elephant()
# e.make_sound()
