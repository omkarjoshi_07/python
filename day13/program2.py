# Method Overriding
class Person:
    def __init__(self, name):
        self._name = name

    def print_details(self):
        print(f"Name : {self._name}")


class Employee(Person):
    def __init__(self, name, id):
        Person.__init__(self, name)
        self.__id = id

    def print_details(self):
        # self.print_person_details()
        # Person.print_person_details()
        print(f"Employee Id : {self.__id}")
        Person.print_details(self)


e1 = Employee("xyz", 202)
e1.print_details()
