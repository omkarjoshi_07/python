class Person:
    def __init__(self, name, age, address):
        self._name = name
        self._age = age
        self._address = address

    def print_person_details(self):
        print(f"Name : {self._name}")
        print(f"Age : {self._age}")
        print(f"Address : {self._address}")


class Student(Person):
    def __init__(self, id, company, name, age, address):
        Person.__init__(self, name, age, address)
        self.__id = id
        self.__company = company

    def print_emp_details(self):
        print("Employee details..")
        self.print_person_details()
        print(f"Id : {self.__id}")
        print(f"Company : {self.__company}")


class Player(Person):
    def __init__(self, name, age, address, team):
        self.__team = team
        Person.__init__(self, name, age, address)

    def print_player_details(self):
        print("Player details ...")
        self.print_person_details()
        print(f"Team : {self.__team}")


# Student.__init(s1, "xyz, 26, "Pune")
# s1 = Student("xyz", 26, "Pune")
# s1.print_details()

# s1 = Student(101, "asdhfds", "xyz", 26, "Pune")
# s1.print_emp_details()

p1 = Player("anc", 26, "Mumbai", "manchester United")
p1.print_player_details()
