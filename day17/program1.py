def function_1():
    # list of numbers
    numbers = [10, 0, 4, 67, 90, 54, 76, 36]
    for n in numbers:
        print(n)


# function_1()

class Person:
    def __init__(self):
        self.__name = "xyz"
        self.__age = 26
    def print_details(self):
        print(f"Name : {self.__name}")
        print(f"Age : {self.__age}")

p1 = Person()
p2 = Person()
person_list = [p1, p2]



def function_2():
    numbers = [10, 0, 4, 67, 90, 54, 76, 36]

    iterator = iter(numbers)
    print(f"iterator : {iterator}")

    tuple_1 = ("Mango", "apple", 400.00, 500)
    iterator_2 = iter(tuple_1)
    print(f"iterator_2 : {iterator_2}")

    set_1 = {12, 34, 545, 'xyz'}
    iterator_3 = iter(set_1)
    print(f"iterator_3 : {iterator_3}")

    dict_1 = {"ID" : 101, "Location" : "Pune", "Native place" : "Kolhapur"}
    iterator_4 = iter(dict_1)
    print(f"iterator_4 : {iterator_4}")

    iterator_5  =iter(person_list)
    print(f"iterator_5 : {iterator_5}")
    

# function_2()

def function_3():
    numbers = [2, 4, 6, 8, 9]
    iterator_1 = iter(numbers)
    try:
        # print(f"Value = {next(iterator_1)}")
        # print(f"Value = {next(iterator_1)}")
        # print(f"Value = {next(iterator_1)}")
        # print(f"Value = {next(iterator_1)}")
        # print(f"Value = {next(iterator_1)}")
        # print(f"Value = {next(iterator_1)}")
        for ele in range(0, 5):
            print(f"Value = {next(iterator_1)}")
    except StopIteration:
        print("Maximum limit of list is reached..")


function_3()