class Person:
    def __init__(self, name, address):
        self._name = name
        self._address = address

    def __str__(self):
        return f"[Name] : {self._name}, [Address] : {self._address}"

class Player(Person):
    def __init__(self, name, address, team):
        self.__team = team
        Person.__init__(self, name, address)
    def __str__(self):
        return f"[Name] : {self._name}, [Address] : {self._address}, [Team] : {self.__team}"



if __name__ == "__main__":
    p1 = Person("raj", "Pune")
    print(p1)