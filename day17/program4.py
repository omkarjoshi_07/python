print(f"Name of module we are using : {__name__}")

def function_1():
    # To use maths.py module we can import whole module
    import calculation
    num1 = int(input("Enter 1st no: "))
    num2 = int(input("Enter 2nd no: "))
    # print(f"Addition : {math.add(num1, num2)}")
    calculation.add(num1, num2)


# function_1()

# __name__() gives name of current module we are using

def function_2():

    # import only add function from calculation module
    from calculation import add
    num1 = int(input("Enter 1st no: "))
    num2 = int(input("Enter 2nd no: "))
    # print(f"Addition : {math.add(num1, num2)}")
    # as we have imported apecific add functionality from calculation module we don't have to write calculation.add()
    add(num1, num2)


# function_2()

def function_3():
    import calculation
    # dir() method gives all methods availabe in calculation module
    # This functionality gives developer information about methods available in the module which he is going to use.
    print(f"contents of math : {dir(calculation)}")

# function_3()

def function_4():
    from calculation import divide,multiply
    multiply(5, 6)
    divide(2, 4)


# function_4()


def function_5():
    # import modeule with alias name
    # If we are having same function as add as in calculation module alias will help us differentiate
    # between that function
    import calculation as cal
    cal.add(20, 56)


function_5()