import random

class Student:
    def __init__(self, roll_no, name, std):
        self.__roll_no = roll_no
        self.__name = name
        self.__std = std
        
    def print_details(self):
        print(f"Roll No : {self.__roll_no}")
        print(f"Name : {self.__name}")
        print(f"Standard : {self.__std}")

    def __str__(self):
        return f"Roll no : {self.__roll_no}, Name : {self.__name}, Stdandard : {self.__std}"


class School:
    def __init__(self, name):
        self.__name = name
        self.__students = []
        self.__index = 0
    def add_student(self, name, std):
        roll_no = len(self.__students) + 1
        student = Student(roll_no, name, std)
        self.__students.append(student)
    def print_details(self):
        print(f"School Name : {self.__name}")
        print("*************Students are*************")
        for stud in self.__students:
            print(f"Roll No : {stud}")
    def __iter__(self):
        return self
    def __next__(self):
        if self.__index >= len(self.__students):
            raise StopIteration
        student = self.__students[self.__index]
        self.__index += 1
        return student

school = School("DKTE")
school.add_student("xyz", 4)
school.add_student("lmn", 5)
school.add_student("sdsa", 6)
school.add_student("asd",10)
# school.print_details()
# here school is not an iterable hence it will give error as 'School' object is not iterable
# for stud in school:?
#     print(stud)

# here also it will give same error as as above
# so  its like this ->school.__iter__()
# School.__iter__()
iterator = iter(school)
# print(f"iterator = {iterator}")
# print(f"Student = {next(iterator)}")
# print(f"Student = {next(iterator)}")
# print(f"Student = {next(iterator)}")
# print(f"Student = {next(iterator)}")

# here as we have overriden __next()__ and __iter()__ method we can now use for loop directly on student object
for student in school:
    print(student)




# here what we can do is we can override the __iter__() method in the school class to make that class iterable

