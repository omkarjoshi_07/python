class Employee:
    def __init__(self, empid, name):
        self.__empid = empid
        self.__name = name
    def __str__(self):
        return f"Name : {self.__name}, Empid: {self.__empid}"

class Company_iterator:
    def __init__(self, employees):
        self.__index = 0
        self.__employees = employees
    def __next__(self):
        if self.__index >= len(self.__employees):
            raise StopIteration()
        emp = self.__employees[self.__index]
        self.__index += 1
        return emp

class Company:
    def __init__(self, name):
        self.__name = name
        self.__index = 0
        self.__employees = []
    def add_employee(self, name):
        empid = len(self.__employees) + 1
        self.__employees.append(Employee(empid, name))
    def __iter__(self):
        return Company_iterator(self.__employees)
    # def __next__(self):
    #     if self.__index >= len(self.__employees):
    #         raise StopIteration
    #     emp = self.__employees[self.__index]
    #     self.__index += 1
    #     return emp


c1 = Company("Infosys")
c1.add_employee("xyz")
c1.add_employee("lfg")
c1.add_employee("gfgfd")
c1.add_employee("gfdfg")

iterator = iter(c1)
print(f"Iterator : {iterator}, type = {type(iterator)}")
for employee in c1:
    print(employee)