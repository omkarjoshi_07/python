def add(p1, p2):
    print(f"Addition : {p1 + p2}")

def divide(p1, p2):
    print(f"Division is : {p1 / p2}")

def multiply(p1, p2):
    print(f"Multiplication : {p1 * p2}")

def substract(p1, p2):
    print(f"Subsrtraction : {p1 - p2}")

# when this module to be imprted to other module then only below code will get executed
# This functionality is useful when one developer has developed this code and other wants to execute only below one.
if __name__ == "__main__":
    add(1, 2)
    print(f"Name of module we are using : {__name__}")
    print(f"Marth modeule code executed..")