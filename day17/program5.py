def function_1():
    import person
    p1 = person.Person("alex", "USA")
    print(p1)

# function_1()

def function_2():
    from person import Player
    p1 = Player("xyz", "Pune", "MI")
    print(p1)

# function_2()

def function_3():
    from person import Player as pl
    # here we are using alias for class name hence we have to use that alias only while creting an object
    player1 = pl("tej", "Mumbai", "RR")
    print(player1)


# function_3()