def function_1():
    """
    finds persons eligible for voting
    """
    persons = [
        {"name": "person1", "email": "p1@test.com", "address": "pune", "age": 10},
        {"name": "person2", "email": "p2@test.com", "address": "mumbai", "age": 18},
        {"name": "person3", "email": "p3@test.com", "address": "satara", "age": 50}
    ]
    eligible_persons = []
    for p in persons:
        if p['age'] > 18:
            eligible_persons.append(p)
    print(f"Eligible persons = {eligible_persons}")

# function_1()


def function_2():
    persons = [
        {"name": "person1", "email": "p1@test.com", "address": "pune", "age": 10},
        {"name": "person2", "email": "p2@test.com", "address": "mumbai", "age": 18},
        {"name": "person3", "email": "p3@test.com", "address": "satara", "age": 50}
    ]
    is_eligible = lambda person : person['age'] >= 18
    eligible_persons = list(filter(is_eligible, persons))
    print(f"Eligible persons = {eligible_persons}")

# function_2()

def function_3():
    """
    finds affordable ccars
    """
    cars = [
        {"model": "i20", "company": "hyundai", "price": 7.5},
        {"model": "i10", "company": "hyundai", "price": 5.5},
        {"model": "fabia", "company": "skoda", "price": 6.5},
        {"model": "nano", "company": "tata", "price": 2.5},
        {"model": "X5", "company": "BMW", "price": 42},
        {"model": "discovery", "company": "range rover", "price": 96}
    ]

    affordable_cars = list(filter(lambda car : car['price'] < 7, cars))
    print(f"Affordable cars = {affordable_cars}")

function_3()

