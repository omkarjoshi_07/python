def even_numbers():
    numbers = list(range(1, 11))
    # print(numbers)
    for n in numbers:
        if n % 2 == 0:
            print(n)


# even_numbers()


def even_numbers2():
    numbers = list(range(1, 11))
    # map will give the result in the form of true or false hence we used filter instead.
    print(f"Even numbers : {list(filter(lambda n: n % 2 == 0, numbers))}")


# even_numbers2()


def function_2():
    numbers = list(range(1, 11))
    evens = []
    for n in numbers:
        if n % 2 == 0:
            evens.append(n)

    print(f"even numbers = {evens}")


# function_2()


def is_even(num):
    return num % 2 == 0


def function_3():
    numbers = list(range(1, 11))

    # map will give output in 0, 1 format as result of true or false from is_even() function
    # even_nos = list(map(is_even, numbers))

    even_nos = list(filter(is_even, numbers))
    print(even_nos)


# function_3()


def function_4():
    """
    finds odd numbers from collection
    """
    numbers = list(range(1, 11))
    # odd_nos = lambda  num: num % 2 != 0
    odds = list(filter(lambda num: num % 2 != 0, numbers))
    print(f"odds = {odds}")


function_4()
