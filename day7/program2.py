def function_1():
    # list of dictionaries
    persons = [
        {"name": "person1", "email": "p1@gmail.com", "age": 24, "address": "Pune"},
        {"name": "person2",  "email": "p2@gmail.com", "age": 25, "address": "Mumbai"},
        {"name": "person3", "age": 21,  "email": "p3@gmail.com", "address": "Kolhapur"}

    ]
    new_persons = []
    #pr int only name and age
    for person in persons:
        new_persons.append({
            "name": person['name'],
            "age": person['age'],
            "email": person['email']
        })

    # print(f"new _persons = {new_persons}")
    # print(new_persons[0])


# function_1()


def function_2():
    persons = [
        {"name": "person1", "email": "p1@gmail.com", "age": 24, "address": "Pune"},
        {"name": "person2", "email": "p2@gmail.com", "age": 25, "address": "Mumbai"},
        {"name": "person3", "age": 21, "email": "p3@gmail.com", "address": "Kolhapur"}

    ]
    transform_person = lambda person: {
        "name": person['name'],
        "age": person['age']
    }

    new_persons = list(map(transform_person, persons))
    print(f"new_persons = {new_persons}")

# function_2()


def function_3():
    cars = [
        {"model": "i20", "company": "hyundai", "price": 7.5},
        {"model": "i10", "company": "hyundai", "price": 5.5},
        {"model": "fabia", "company": "skoda", "price": 6.5},
        {"model": "nano", "company": "tata", "price": 2.5},
        {"model": "X5", "company": "BMW", "price": 42},
        {"model": "discovery", "company": "range rover", "price": 96}
    ]
    transform_car = lambda car : {
        "model" : car['model'],
        "price" : car['price']
    }

    get_cars = list(map(transform_car, cars))
    print(f"cars = {get_cars}")


function_3()


