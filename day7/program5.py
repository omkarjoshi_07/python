def check_even_odd(num):
    if num % 2 == 0:
        return "{0} is even number".format(num)
    else:
        return "{0} is odd number".format(num)


def check_even_odd_of_list_ele():
    numbers = [14, 15, 4, 7, 9, 10]
    checks = list(map(check_even_odd, numbers))
    print(checks)


# check_even_odd_of_list_ele()

# use of map and lambda together


def calculate_square_of_list_ele():
    numbers = [2, 4, 5, 6, 7, 9]
    # here, instead of calling the function we did lambda coding
    squares = list(map(lambda num: num ** 2, numbers))
    print(f"Squares = {squares}")


calculate_square_of_list_ele()


def send_person_info(person):
    return person


def person_info():
    # persons = [
    #     {"Name": "xyz", "company": "Infosys", "Location": "Pune"},
    #     {"Name": "abc", "company": "TCS", "Location": "Pune"}
    # ]
    employees = [
                 {"Name": "xyz", "Company": "ads", "Location": "Pune"},
                 {"Name": "sdsa", "Company": "ads", "Location": "Pune"}
    ]
    info = list(map(lambda emp: {"name": emp['Name'], "Company": emp['Company']}, employees))
    print(info)


person_info()


def car_details():
    cars = [
        {"Model": "Alto 800", "Make": "Maruti SUzuki", "Price": 400000.00},
        {"Model": "Nano", "Make": "TATA", "Price": 350000.00},
        {"Model": "Ecosport", "Make": "Ford", "Price": 700000.00},
        {"Model": "Rapid", "Make": "Skoda", "Price": 800000.00}

    ]
    # we want to print only cars model and make
    cars_info = list(map(lambda car: {"Model": car['Model'], "Price": car['Price']}, cars))
    print(f"cars Info : {cars_info}")


car_details()
