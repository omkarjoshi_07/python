def function_1():
    numbers = [2, 1, 4, 5, 3]
    for n in numbers:
        print(n ** 3)


# function_1()


def function_2():
    numbers = [2, 1, 4, 5, 3]
    squares = []
    for num in numbers:
        squares.append(num ** 2)
    print(squares)


# function_2()


def calculate_square(num):
    print(f"inside calculate_square of {num}")
    return num ** 2


def calculate_cube(num):
    return num ** 3


def calculate_cube_of_list_ele():
    """
        This function calculates cube of all elements using map()
        BTW, map() is used when u want to iterate over a collection do some manipulation
    """
    numbers = [1, 3, 4, 2, 10]
    # calculate cube using for loop
    # cube_1 = []
    # for index in range(len(numbers)):
    #     cube_1.append(calculate_cube(numbers[index]))
    # print(f"Cube of all numbers = {cube_1}")

    # calculate cube of list ele using map
    cubes = list(map(calculate_cube, numbers))
    print(f"Cubes = {cubes}")
    # cubes = map(calculate_cube, numbers)
    # print(f"Cubes = {cubes}")


calculate_cube_of_list_ele()


def function_3():
    numbers = [6, 7, 8, 9, 10]

    # map is iterating 'numbers' collection using for loop i.e. passing each element to function and returns result
    squares = list(map(calculate_square, numbers))
    print(f"squares of all numbers = {squares}")


# function_3()


def function_4():
    numbers = [2, 3, 4, 5, 6]
    cubes = list(map(lambda num: num ** 3, numbers))

    print(f"cubes of all numbers = {cubes}")


# function_4()


def function_5():
    """
    calculates cube of each number from collection
    using lambda and map() function

    """
    numbers = [2, 3, 4, 5, 6]

    cubes = list(map(lambda n: n ** 3, numbers))
    print(f"cubes of all numbers = {cubes}")

# function_5()


def function_6():
    """
    add 10 in each number of collection
    """
    numbers = [2, 3, 4, 5, 6]
    print(f"numbers after 10 added = {list(map(lambda n : n + 10, numbers))}")


# function_6()
