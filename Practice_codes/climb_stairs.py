def function_1():
    n = 6
    iterations = []
    # print(range.__doc__)
    for i in range(0, n):
        iterations.append(1)

    # return iterations
    cntr = 1
    for i in range(0, len(iterations)):
        iterations[i] = 2
        result = iterations[i]
        while result != n:
            i += 1
            result += iterations[i]

        cntr += 1


    return sum(iterations, 2)


print(function_1())
