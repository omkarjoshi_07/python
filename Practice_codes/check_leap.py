def is_leap(year):
    leap = ""

    # Write your logic here
    if year % 4 == 0:
        if year % 100 == 0:
            if year % 400 == 0:
                return "Leap Year"
        else:
            return "Leap Year"

    return "Not a leap year"


print(is_leap(2000))