# find a ele in list
list_1 = [12, 45, 567,'asda']
# print('asda' in list_1)

# swap 1st and last ele
# temp  = list_1[0]
# list_1[0] = list_1[-1]
# list_1[-1] = temp
# print(list_1)


# 2nd approach
# list_1[0], list_1[-1] = list_1[-1], list_1[0]
# print(list_1)

# pos1 = 1
# pos2 = 3

# list_1[pos1], list_1[pos2]  = list_1[pos2], list_1[pos1]
# print(list_1)


