def roman_to_integer():
    roman_values = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}
    # s = "III"
    # s = "LVIII"
    s = "MCMXCIV"

    res = 0
    for i in range(0, len(s)):
        if i != (len(s) - 1) and roman_values.get(s[i]) < roman_values.get(s[i+1]):
            # print('if true')
            # print(f"{roman_values.get(s[i])}, {roman_values.get(s[i+1])}")
            res += (roman_values.get(s[i+1]) - roman_values.get(s[i]))
            i += 2
            # print(i)
            # print(res)
        else:
            res += roman_values.get(s[i])
            # print(i)

    print(res)


roman_to_integer()

