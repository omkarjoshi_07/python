class Solution:
    def isPalindrome(self, x: int) -> bool:
        str1 = str(x)
        # print(list(str1))
        l1 = list(str1)
        l1.reverse()
        # s2 = ''.join(l1)
        # print(l1)
        # print(int(s2))
        if int(''.join(l1)) == x:
            return True

        return False




s1 = Solution()
print(s1.isPalindrome(123))
