def length_of_last_word():
    str = "hello world asd"
    # str =
    for i in range(-1, 0, -1):
        print(i)
    # print(str.isspace())
    # print(str.index(' '))
    # print(str.count(' '))
    # print(str.find(' '))

    spaces = []
    for i in range(0, len(str)):
        if str[i] == ' ':
            spaces.append(i)

    # print(spaces)
    print(len(str[spaces[-1] + 1:]))


# length_of_last_word()


def function():
    # s = "   fly me   to   the moon  "
    # s = "hello world"
    s = "a"
    s = s.rstrip()
    if len(s) == 1:
        return 1

    spaces = []
    for i in range(0, len(s)):
        if s[i] == ' ':
            spaces.append(i)

    # print(spaces)

    return len(s[spaces[-1] + 1:])


print(function())
