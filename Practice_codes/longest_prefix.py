# find the longest prefix in a list of strings

def longest_prefix():
    # strs = ["flow", "flower", "flight"]
    # strs = ["dog", "racecar", "car"]
    strs = ["cir", "car"]
    # s = strs[0]
    # print(f"s : {s}")
    cntr = 1
    l1 = []
    flag = False
    for char in strs[0]:
        # print(f"{char} is finding....")
        for i in range(1, len(strs)):
            # print(strs[i])
            if char in strs[i]:
                # print(f"{char} is found")
                # print(strs[i].find(char))
                cntr += 1
            else:
                flag = True

        if flag:
            break

        if cntr == len(strs):
            # print(cntr)
            l1.append(char)
        cntr = 1
    return l1


# print(longest_prefix())


def func1():
    # strs = ["dog", "racecar", "car"]
    # strs = ['a']
    # strs = ["ab", "a"]
    strs = ["c", "acc", "ccc"]
    # strs = ["flow", "flower", "flight"]
    # strs = ["aaa", "aa", "aaa"]
    # strs = ["abab", "aba", "abc"]
    # strs = ["c", "acc", "ccc"]
    # strs = ["ab", "ab"]

    # strs2 = strs[1:]
    # print(strs2)
    s = strs[0]
    j = len(s) - 1
    cntr = 1
    l1 = []
    if len(strs) == 1:
        print("length is not 1")
        return strs[0]

    for i in range(0, len(s)):

        # print(s[0:j + 1])
        for k in range(1, len(strs)):
            if s[0:j + 1] in strs[k]:
                print(f"{s[0:j + 1]} found")
                cntr += 1
                # print(cntr)
            else:
                cntr = 1
                print(f"{s[0:j + 1]} not found...\n breaking the inner for loop")
                break
        if cntr == len(strs):
            l1.append(s[0: j + 1])
            print(f"{s[0:j + 1]} is appended to list{l1}")

        print(f"j decremented ..")
        j -= 1

    # print(len(l1))
    # if l1 == []:
    #     print('list is empty')

    if l1 != []:
        if len(l1[0]) == 0 or len(l1[0]) == 1:
            return l1[0]
    return l1


print(func1())
