# print("*" * 5)
n = 1

def pattern1(n):
    for i in range(5):
        print("*" * n)
        n = n + 1

# pattern1(n)

def pattern2(n):
    for i in range(5):
        print("*" * n)
        n = n + 1

# pattern2(n)

def pattern3(n):
    for i in range(5):
        print("*" * n)
        n = n - 1

pattern3(5)